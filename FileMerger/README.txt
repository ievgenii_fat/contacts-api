The application designed for merging two CSV file into one.
After launching the application choose from menu action you want to do (1 - to merge files, 2 - to exit).
When you select 1 option the program will ask you to enter path to first file, then for the second. After pressing "Enter" program will merge two files. Therefore you'll see output if operation succeeded or failed.
The format of files is standard CSV format, values separated by comma.
There is one more file "columns.csv". Here you input column names by which the program will merge files. Each column name should be in separate line.

Files examples:
#######################################################
Example 1:
===================== file1.csv =====================
Customer id,Customer name,Customer email id,Customer Phone number,Customer City1234,XYZ,xyz@test.net,213456789,CA

===================== file2.csv =====================
Customer id,Customer ZIP code,Order ID1234,12345,43214

===================== columns.csv =====================
Customer id

#######################################################
Example 2:
===================== file1.csv =====================
Customer id,Customer name,Customer email id,Customer Phone number,Customer City1234,XYZ,xyz@test.net,213456789,CA

===================== file2.csv =====================
Customer id,Customer name,Customer email id,Customer Phone number,Customer City4567,ABC,abc@test.net,21456734,CA

===================== columns.csv =====================
Customer id

#######################################################
Example 3:
===================== file1.csv =====================
Customer id,Customer name,Customer email id,Customer Phone number,Customer City1234,XYZ,xyz@test.net,213456789,CA
1234,ZZZ,ZZZ@test.net,111111111,FL

===================== file2.csv =====================
Customer id,Customer email id,Customer ZIP code,Order ID1234,abc@test.net,12345,43214
1234,111@test.net,11111,11111

===================== columns.csv =====================
Customer id
Customer email id

#######################################################