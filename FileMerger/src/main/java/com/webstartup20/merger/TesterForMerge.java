package com.webstartup20.merger;

import com.webstartup20.csv.CsvController;

import java.util.*;

/**
 * Created by Sergiy on 05-Jan-15.
 */
public class TesterForMerge {//TODO remove me(class), please!
    public void test(List<Map<String, String>> contacts1, List<Map<String, String>> contacts2) {

        Merger merger = new Merger(contacts1, contacts2);
        merger.merge(Arrays.asList("b"));

        System.out.println(CsvController.convertToListOfMaps(merger.getMergedDataNDup()));
    }

    public static void main(String[] args) {
        List<Map<String, String>> c1 = new ArrayList<>();
        Map<String, String> m11 = new HashMap<>();
        Map<String, String> m12 = new HashMap<>();
        m11.put("a","val11");m11.put("b","val12");m11.put("c","val13");
        m12.put("a","val21");m12.put("b","val22");m12.put("c","val23");

        c1.add(m11);
        c1.add(m12);

        List<Map<String, String>> c2 = new ArrayList<>();
        Map<String, String> m22 = new HashMap<>();
        m22.put("d","lav11");m22.put("b","val12");
        c2.add(m22);

        System.out.println(c1);
        System.out.println(c2);
        TesterForMerge d = new TesterForMerge();
        d.test(c1,c2);
    }
}
