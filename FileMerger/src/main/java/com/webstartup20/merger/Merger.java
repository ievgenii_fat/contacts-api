package com.webstartup20.merger;

import com.webstartup20.csv.CsvController;
import com.webstartup20.csv.CsvUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.util.*;

public class Merger {


    private String[] headers1;
    private String[] headers2;
    //headers that will be used to merge by
    private List<String> mergeParams;
    //    private ArrayList<String> sameHeaders;
    private List<Integer> header1Index;
    private List<Integer> header2Index;
    //headers of final table
    private String[] mergedHeaders;
    private String[][] data1;//data from file 1
    private String[][] data2;// data from file 2

    private List<String> csv1;
    private List<String> csv2;
    private List<String[]> mergedDataDup;
    private List<String[]> mergedDataNDup;

    public Merger(List<String> fileData1, List<String> fileData2) {
        csv1 = fileData1;
        csv2 = fileData2;
        parseData(csv1, csv2);
    }

    public Merger(Collection<Map<String, String>> contacts1, Collection<Map<String, String>> contacts2) {
//        this.mergeParams = Arrays.asList(mergeParams);
        headers1 = CsvController.createHeader(contacts1);
        headers2 = CsvController.createHeader(contacts2);
        data1 = CsvController.createDataArray(contacts1);
        data2 = CsvController.createDataArray(contacts2);
    }

    public void merge(List<String> mergeParams) {
        mergedDataDup = new ArrayList<>();
        mergedDataNDup = new ArrayList<>();

        this.mergeParams = new ArrayList<>(mergeParams);
        mergedHeaders = mergeHeaders();
        printColumnsToMerge();
        defineMergeIdx();
        mergeData();

        System.out.println("first file: " + data1.length);
        System.out.println("second file: " + data2.length);
        System.out.println("merged: " + mergedDataNDup.size());
        System.out.println("duplicates: " + mergedDataDup.size());

        if (mergedDataDup.size() > 0)
            mergedDataDup.add(0, mergedHeaders);

        if (mergedDataNDup.size() > 0)
            mergedDataNDup.add(0, mergedHeaders);

    }

    public void printColumnsToMerge() {
        System.out.print("Columns: ");
        for (int i = 0; i < mergeParams.size(); i++) {
            System.out.print(mergeParams.get(i));
            if (i != mergeParams.size() - 1)
                System.out.print(", ");
        }
        System.out.println();
    }

    public List<String[]> getMergedDataDup() {
        return mergedDataDup;
    }

    public List<String[]> getMergedDataNDup() {
        return mergedDataNDup;
    }

    private void parseData(List<String> csv1, List<String> csv2) {

        headers1 = CsvUtil.parseCsvRow(csv1.get(0));
        headers2 = CsvUtil.parseCsvRow(csv2.get(0));
//        System.out.println(Arrays.toString(headers1));
//        System.out.println(Arrays.toString(headers2));
        data1 = getCSVData(csv1.subList(1, csv1.size()));
        data2 = getCSVData(csv2.subList(1, csv2.size()));
    }

    private String[][] getCSVData(List<String> data) {
        String[][] csvData = new String[data.size()][headers1.length];
        int j = 0;
        for (int i = 0; i < csvData.length; i++) {
            if (j < data.size()) {
                csvData[i] = CsvUtil.parseCsvRow(data.get(j));
                j++;
            }
//            System.out.println(Arrays.toString(csvData[i]));
        }
        return csvData;
    }

    private String[] mergeHeaders() {

        ArrayList<String> h1 = new ArrayList<>(Arrays.asList(headers1));
        ArrayList<String> h2 = new ArrayList<>(Arrays.asList(headers2));
        LinkedHashSet<String> merged = new LinkedHashSet<>();
        merged.addAll(h1);
        merged.addAll(h2);
        return merged.toArray(new String[merged.size()]);

    }


    private void append() {
        for (int i = 0; i < data1.length; i++) {
            mergedDataNDup.add(data1[i]);
        }
        for (int i = 0; i < data2.length; i++) {
            mergedDataNDup.add(data2[i]);
        }
        HashMap<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < mergedDataNDup.size(); i++) {
            List<Integer> values = new ArrayList<>();
            for (int j = 1; j < mergedDataNDup.size(); j++) {
                if (isParamsMatch(mergedDataNDup.get(i), mergedDataNDup.get(j))) {
                    if (!values.contains(j) && i != j) values.add(j);
                }
            }
            if (values.size() > 0)
                map.put(i, values);
        }

        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            int item = Integer.parseInt(pairs.getKey().toString());
            List<Integer> values = (ArrayList<Integer>) pairs.getValue();
            if (mergedDataNDup.get(item) != null) {
                if (mergedDataDup.size() == 0) {
                    mergedDataDup.add(mergedDataNDup.get(item));
                } else {
                    if (!containsRow(mergedDataDup, mergedDataNDup.get(item)))
                        mergedDataDup.add(mergedDataNDup.get(item));
                }
                mergedDataNDup.set(item, null);
            }
            for (int i = 0; i < values.size(); i++) {
                if (mergedDataNDup.get(values.get(i)) != null) {
                    if (mergedDataDup.size() == 0) {
                        mergedDataDup.add(mergedDataNDup.get(values.get(i)));
                    } else {
                        if (!containsRow(mergedDataDup, mergedDataNDup.get(values.get(i))))
                            mergedDataDup.add(mergedDataNDup.get(values.get(i)));
                    }
                    mergedDataNDup.set(values.get(i), null);
                }
            }
        }

        for (int i = 0; i < mergedDataNDup.size(); i++) {
            if (mergedDataNDup.get(i) == null) {
                mergedDataNDup.remove(i);
                i--;
            }
        }
    }

    private void mergeData() {
        if (isHeadersEquals()) {
            append();
        } else {
            mergeDifferentHeaders();
        }
    }

    private void removeDupsFromData1() {

        List<String[]> d1 = new ArrayList<>();
        for (int i = 0; i < data1.length; i++) {
            d1.add(data1[i]);
        }

        HashMap<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < d1.size(); i++) {
            List<Integer> values = new ArrayList<>();
            for (int j = 1; j < d1.size(); j++) {
                if (isParamsMatch(d1.get(i), d1.get(j), header1Index)) {
                    if (!values.contains(j) && i != j) values.add(j);
                }
            }
            if (values.size() > 0)
                map.put(i, values);
        }

        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            int item = Integer.parseInt(pairs.getKey().toString());
            List<Integer> values = (ArrayList<Integer>) pairs.getValue();
            String[] empty1 = new String[mergedHeaders.length];

            if (d1.get(item) != null) {
                for (int k = 0; k < d1.get(item).length; k++) {
                    empty1[getIndex1InMergedTable(k)] = d1.get(item)[k];
                }
                for (int k = 0; k < empty1.length; k++) {
                    if (empty1[k] == null) empty1[k] = "";
                }
                if (mergedDataDup.size() == 0) {
                    mergedDataDup.add(empty1);
                } else {
                    if (!containsRow(mergedDataDup, empty1))
                        mergedDataDup.add(empty1);
                }
                d1.set(item, null);
            }

            for (int i = 0; i < values.size(); i++) {
                String[] empty = new String[mergedHeaders.length];
                if (d1.get(values.get(i)) != null) {
                    for (int k = 0; k < d1.get(values.get(i)).length; k++) {
                        empty[getIndex1InMergedTable(k)] = d1.get(values.get(i))[k];
                    }
                    for (int k = 0; k < empty.length; k++) {
                        if (empty[k] == null) empty[k] = "";
                    }
                    if (mergedDataDup.size() == 0) {
                        mergedDataDup.add(empty);
                    } else {
                        if (!containsRow(mergedDataDup, empty))
                            mergedDataDup.add(empty);
                    }
                    d1.set(values.get(i), null);
                }
            }
        }

        for (int i = 0; i < d1.size(); i++) {
            if (d1.get(i) == null) {
                d1.remove(i);
                i--;
            }
        }
        if (d1.size() > 0) {
            data1 = new String[d1.size()][d1.get(0).length];
            for (int i = 0; i < d1.size(); i++) {
                data1[i] = d1.get(i);
            }
        } else {
            data1 = new String[0][0];
        }

    }


    private void removeDupsFromData2() {

        List<String[]> d2 = new ArrayList<>();
        for (int i = 0; i < data2.length; i++) {
            d2.add(data2[i]);
        }

        HashMap<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < d2.size(); i++) {
            List<Integer> values = new ArrayList<>();
            for (int j = 1; j < d2.size(); j++) {
                if (isParamsMatch(d2.get(i), d2.get(j), header2Index)) {
                    if (!values.contains(j) && i != j) values.add(j);
                }
            }
            if (values.size() > 0)
                map.put(i, values);
        }
        System.out.println(void.class);
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            int item = Integer.parseInt(pairs.getKey().toString());
            List<Integer> values = (ArrayList<Integer>) pairs.getValue();
            String[] empty2 = new String[mergedHeaders.length];

            if (d2.get(item) != null) {
                for (int k = 0; k < d2.get(item).length; k++) {
                    empty2[getIndex2InMergedTable(k)] = d2.get(item)[k];
                }
                for (int k = 0; k < empty2.length; k++) {
                    if (empty2[k] == null) empty2[k] = "";
                }
                if (mergedDataDup.size() == 0) {
                    mergedDataDup.add(empty2);
                } else {
                    if (!containsRow(mergedDataDup, empty2))
                        mergedDataDup.add(empty2);
                }
                d2.set(item, null);
            }

            for (int i = 0; i < values.size(); i++) {
                String[] empty = new String[mergedHeaders.length];
                if (d2.get(values.get(i)) != null) {
                    for (int k = 0; k < d2.get(values.get(i)).length; k++) {
                        empty[getIndex2InMergedTable(k)] = d2.get(values.get(i))[k];
                    }
                    for (int k = 0; k < empty.length; k++) {
                        if (empty[k] == null) empty[k] = "";
                    }
                    if (mergedDataDup.size() == 0) {
                        mergedDataDup.add(empty);
                    } else {
                        if (!containsRow(mergedDataDup, empty))
                            mergedDataDup.add(empty);
                    }
                    d2.set(values.get(i), null);
                }
            }
        }

        for (int i = 0; i < d2.size(); i++) {
            if (d2.get(i) == null) {
                d2.remove(i);
                i--;
            }
        }
        if (d2.size() > 0) {
            data2 = new String[d2.size()][d2.get(0).length];
            for (int i = 0; i < d2.size(); i++) {
                data2[i] = d2.get(i);
            }
        } else {
            data2 = new String[0][0];
        }
    }

    private void mergeDifferentHeaders() {
        removeDupsFromData1();
        removeDupsFromData2();
        for (int i = 0; i < data1.length; i++) {
            for (int j = 0; j < data2.length; j++) {
                if (isParamsMatch(data1[i], data2[j])) {
                    if (!containsRow(mergedDataNDup, mergeRows(data1[i], data2[j]))) {
                        mergedDataNDup.add(mergeRows(data1[i], data2[j]));
                    }
                }
            }
        }
    }

    private boolean isHeadersEquals() {
        if (headers1.length == headers2.length) {
            for (int i = 0; i < headers1.length; i++) {
//                if (!headers1[i].replace(" ", "").equalsIgnoreCase(headers2[i].replace(" ", ""))) {
                if (!equalsWithoutSpaces(headers1[i], headers2[i])) {
                    return false;
                }
            }
        } else {
            return false;

        }
        return true;
    }

    private int getIndex1InMergedTable(int idx) {
        String header = headers1[idx];
        for (int i = 0; i < mergedHeaders.length; i++) {
            if (mergedHeaders[i].equals(header)) return i;
        }
        return 0;
    }

    private int getIndex2InMergedTable(int idx) {
        String header = headers2[idx];
        for (int i = 0; i < mergedHeaders.length; i++) {
            if (mergedHeaders[i].equals(header)) return i;
        }
        return 0;
    }

    private boolean containsRow(List<String[]> data, String[] row) {
        boolean contains = false;
        for (int i = 0; i < data.size(); i++) {
            int count = 0;
            for (int j = 0; j < data.get(i).length; j++) {
                if (data.get(i)[j].equals(row[j])) {
                    count++;
                }
            }
            if (count == data.get(i).length) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    private String[] mergeRows(String[] row1, String[] row2) {
        String[] m = new String[mergedHeaders.length];
        for (int i = 0; i < row1.length; i++) {
            m[getIndex1InMergedTable(i)] = row1[i];
        }
        for (int i = 0; i < row2.length; i++) {
            m[getIndex2InMergedTable(i)] = row2[i];
        }
        return m;
    }


    private boolean isParamsMatch(String[] row1, String[] row2) {
        boolean totalMatch = true;

        for (int i = 0; i < header1Index.size(); i++) {
            if (!row1[header1Index.get(i)].replace(" ", "").equalsIgnoreCase(row2[header2Index.get(i)].replace(" ", ""))) {
//            if (!equalsWithoutSpaces(row1[header1Index.get(i)], row2[header2Index.get(i)])) {
                totalMatch = false;
            }
        }
        return totalMatch;
    }

    private boolean isParamsMatch(String[] row1, String[] row2, List<Integer> headerIndex) {
        boolean totalMatch = true;
        for (int i = 0; i < headerIndex.size(); i++) {
            if (!row1[headerIndex.get(i)].replace(" ", "").equalsIgnoreCase(row2[headerIndex.get(i)].replace(" ", ""))) {
//            if (!equalsWithoutSpaces(row1[header1Index.get(i)], row2[header2Index.get(i)])) {
                totalMatch = false;
            }
        }
        return totalMatch;
    }

    private void defineMergeIdx() {
        header1Index = new ArrayList<>();
        header2Index = new ArrayList<>();
        for (int i = 0; i < headers1.length; i++) {
            for (int j = 0; j < mergeParams.size(); j++) {
                if (equalsWithoutSpaces(headers1[i], mergeParams.get(j))) {
//                if (headers1[i].equals(mergeParams.get(j))) {
                    header1Index.add(i);
                }
            }
        }
        for (int i = 0; i < headers2.length; i++) {
            for (int j = 0; j < mergeParams.size(); j++) {
                if (equalsWithoutSpaces(headers2[i], mergeParams.get(j))) {
                    header2Index.add(i);
                }
            }
        }
    }

    private boolean equalsWithoutSpaces(String s1, String s2) {
        return s1.replace(" ", "").equalsIgnoreCase(s2.replace(" ", ""));
    }

}

