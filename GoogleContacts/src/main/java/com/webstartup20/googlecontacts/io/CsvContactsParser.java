package com.webstartup20.googlecontacts.io;

import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.extensions.*;
import com.webstartup20.googlecontacts.utils.ContactEntryUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class CsvContactsParser {

    public static final String SEPARATOR = ",";

    private static final String FIRST_NAME = "first name";
    private static final String FAMILY_NAME = "family name";
    private static final String EMAILS = "emails";
    private static final String EMPTY = "";
    private static final String[] HEADER_NAMES = {FIRST_NAME, FAMILY_NAME, EMAILS, EMPTY};


    private Map<Integer, String> header;
    private List<ContactEntry> contacts;
    private List<Integer> invalidContacts;

    public CsvContactsParser(String filePath){
        this(new File(filePath));
    }

    public CsvContactsParser(File file){
        contacts = new ArrayList<>();
        invalidContacts = new ArrayList<>();
        header = new HashMap<>();
        readContacts(file);
    }

    private void readContacts(File file){

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            processHeader(br.readLine());// read header
            ContactEntry currentContact = null;
            int count = 0;
            while ((line = br.readLine()) != null) {

                if(isEmptyRow(line)){
                    continue;
                }
                currentContact = parseContactRow(line);
                if(currentContact!=null){
                    contacts.add(currentContact);
                }else{
                    invalidContacts.add(count);
                    count++;
                }
            }
        }catch (FileNotFoundException e){
            System.err.format("File '%s' not found", file.getAbsolutePath());
        }catch (IOException e){
            System.err.format("Error during reading file '%s'", file.getAbsolutePath());
        }
    }

    private void processHeader(String row){
        String[] headerArray = row.split(SEPARATOR);
        for (int i = 0; i < headerArray.length; i++) {
            if(containsValue(headerArray[i], HEADER_NAMES)){
                header.put(i, headerArray[i].toLowerCase());
            }else if(headerArray[i].replace(SEPARATOR,"").length() <= 0){
                header.put(i, "");
            }else{
                System.err.println(String.format("'%s' field did not determined"));
            }
        }
    }

    private boolean containsValue(String value, String[] array){
        for (int i = 0; i < array.length; i++) {
            if(array[i].equalsIgnoreCase(value)){
                return true;
            }
        }
        return false;
    }

    private boolean isEmptyRow(String row){
        String buff = row.replace(SEPARATOR, "");
        buff = buff.replace(" ", "");
        return buff.length() == 0;
    }

    private ContactEntry parseContactRow(String row){
        String[] splittedRow = row.split(SEPARATOR);

        String givenName = "";
        String familyName = "";
        String email = "";

        for (int i = 0; i < splittedRow.length; i++) {
            switch (header.get(i)){
                case FIRST_NAME:
                    givenName = splittedRow[i];
                    break;
                case FAMILY_NAME:
                    familyName = splittedRow[i];
                    break;
                case EMAILS:
                    email = splittedRow[i];
                    break;
                case EMPTY:
                    break;
            }
        }

        if(isEmptyRow(givenName) || isEmptyRow(familyName) || !ContactEntryUtil.validate(email)){
            System.err.println(String.format("Contact [first name:'%s', last name:'%s', email:'%s'] was skipped.",givenName,familyName,email));
            return null;
        }
        return ContactEntryUtil.createContact(givenName, familyName, email);
    }


    public List<ContactEntry> getContacts(){
        return contacts;
    }
    public List<Integer> getInvalidContacts(){
        return invalidContacts;
    }




    public static void main(String[] args) {
        CsvContactsParser c = new CsvContactsParser("sample_data.csv");
        List<ContactEntry> contactEntries = c.getContacts();

        for (ContactEntry contactEntry: contactEntries){
            System.out.println(contactEntry.getName().getGivenName().getValue());
            System.out.println(contactEntry.getName().getFamilyName().getValue());
            List<Email> emails = contactEntry.getEmailAddresses();
            for (Email email: emails){
                System.out.println(email.getAddress());
            }
            System.out.println("-------------------------------------------------");
        }
        System.out.println("Total: "+contactEntries.size());
    }
}
