package com.webstartup20.googlecontacts;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.gdata.client.Query;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import com.webstartup20.api_controller.AController;
import com.webstartup20.api_controller.exception.InvalidControllerNameException;
import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.googlecontacts.utils.ContactEntryUtil;
import com.webstartup20.googlecontacts.utils.GoogleDataUtils;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.parsers.ContactParser;
import com.webstartup20.struct.IContact;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * To change the method of authorization(command line or browser)
 * just change the constructor of the ContactsDownloader
 */
public class GoogleController extends AController {

    private String login;
    private String password;

    private String accessToken;

    public static final String URL_FOR_FEED = "https://www.google.com/m8/feeds/contacts/default/full";
    public static final String PERMISSIONS_PATH = "permissions/plus_sample/StoredCredential";
    public static final String APP_NAME = "webstartup20-contactsRetriever-1.0";
    private ContactFeed resultFeed;

    public GoogleController(int id, String login, String password) {
        super(id);
        this.login = login;
        this.password = password;
    }
    public GoogleController(int id, String accessToken) {
        super(id);
        this.accessToken=accessToken;
    }
    /**
     * Method for browser authorization
     */
    /**
     * private void downloadContacts() {
     * try {
     * ContactsService myService = new ContactsService(APP_NAME);
     * myService.setOAuth2Credentials(AuthorizationHelper.authorize());
     * resultFeed = getContactFeed(myService);
     * contacts = getContactList(resultFeed);
     * } catch (ServiceException e) {
     * System.err.println("Error authorizing your account with Google");
     * } catch (IOException e) {
     * System.err.println("Error reading data from remote source");
     * } catch (Exception e) {
     * System.err.println("Some problem occurred. Check your connection and input parameters");
     * } finally {
     * FileUtils.deleteFile(PERMISSIONS_PATH);
     * }
     * }
     */
    private ContactFeed getContactFeed(ContactsService myService) throws ServiceException, IOException {
        URL feedUrl = new URL(URL_FOR_FEED);
        Query myQuery = new Query(feedUrl);
        myQuery.setMaxResults(Integer.MAX_VALUE);
        ContactFeed resultFeed = myService.getFeed(myQuery, ContactFeed.class);
        return resultFeed;
    }

    @Override
    public RestEntity importContacts(List<Map<String, String>> contacts, FieldMapperBuilder fieldMapperBuilder){

        String exportControllerName;
        try {
            exportControllerName = determineExportController(fieldMapperBuilder);
        } catch (InvalidControllerNameException e) {
            int errorId = MessagesPrinter.getInstance().printErrorMessage(this.getClass(), e.getMessage());
            String errorMessage = String.format("[ID: %s] Internal server error. Please inform your administrator!", errorId);
            return generateResponse(errorMessage, 0, 0);
        }

        ContactParser contactParser = ContactParser.build(
                exportControllerName,
                getControllerName(),
                fieldMapperBuilder.getFieldsMappers());

        contactParser.parseData(contacts);
        int success = importContacts(contactParser.getContacts());

        return generateResponse("success", success, contacts.size() - success);
    }

    private int importContacts(List<IContact> contacts) {
        int success = 0;
        for (IContact contact : contacts) {
            ContactEntry contactEntry = ContactEntryUtil.createContact(contact.getFirstName(), contact.getLastName(), contact.getEmails());
            try {
                URL feedUrl = new URL(URL_FOR_FEED);
                ContactsService myService = new ContactsService(APP_NAME);
//                myService.setUserCredentials(login, password);
                myService.setHeader("Authorization", "Bearer " + accessToken);
                myService.insert(feedUrl, contactEntry);
                success++;
            } catch (IOException e) {
                MessagesPrinter.getInstance().printErrorMessage(this.getClass(),"Failed to import contacts, please check your internet connection");
                System.out.println(contact);
                e.printStackTrace();
            } catch (ServiceException e) {
                MessagesPrinter.getInstance().printErrorMessage(this.getClass(),"Failed to import contacts, please check input parameters");
                System.out.println(contact);
                e.printStackTrace();
            }
        }
        return success;
    }



    @Override
    public List<Map<String, String>> exportContacts() {
        try {
            ContactsService myService = new ContactsService(APP_NAME);
//            myService.setUserCredentials(login, password);
            myService.setHeader("Authorization", "Bearer " + accessToken);
            return GoogleDataUtils.getContactList(getContactFeed(myService));
        } catch (AuthenticationException e) {
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(),"Authentication error. Incorrect email or password. Also check your security settings at https://www.google.com/settings/security/lesssecureapps");
        } catch (ServiceException e) {
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(),"Error while processing a GDataRequest");
        } catch (IOException e) {
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(),"Error while reading data from server");
        }

//        return Collections.emptyList();
        return Collections.emptyList();
    }

    @Override
    public String getControllerName() {
        return ResourcesConstants.Google.NAME;
    }

    public static void main(String[] args) {
        GoogleController googleController = new GoogleController(1,
                "ya29.CAFS26C4aKn_djFoKJ6DZ8tdQQ-xUHD6SnBgvWtLl0Jx0-IQs2EC6aZUppPZf5CCBQP3Le-61u8ToQ");
        googleController.exportContacts();


    }

}
