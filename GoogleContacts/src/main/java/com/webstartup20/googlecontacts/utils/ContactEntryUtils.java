package com.webstartup20.googlecontacts.utils;

import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.GroupMembershipInfo;
import com.google.gdata.data.extensions.Email;
import com.google.gdata.data.extensions.ExtendedProperty;
import com.google.gdata.data.extensions.Im;

import java.util.ArrayList;
import java.util.List;

public class ContactEntryUtils {

    public static String getEtag(ContactEntry entry) {
        return entry.getEtag();
    }

    public static String getPhotoLink(ContactEntry entry) {
        return entry.getContactPhotoLink().getHref();
    }

    public static String getExtendedProperties(ContactEntry entry) {
        for (ExtendedProperty property : entry.getExtendedProperties()) {
            if (property.getValue() != null) {
                return String.format("%s = %s", property.getName(), property.getValue());
            } else if (property.getXmlBlob() != null) {
                return String.format("%s = %s (xmlBlob)", property.getName(), property.getXmlBlob().getBlob());
            }
        }
        return "";
    }

    public static String getGroups(ContactEntry entry) {
        for (GroupMembershipInfo group : entry.getGroupMembershipInfos()) {
            return group.getHref();
        }

        return "";
    }

    public static String getImAddresses(ContactEntry entry) {
        for (Im im : entry.getImAddresses()) {
            return  im.getAddress();
        }
        return "";
    }

    public static List<String> getEmails(ContactEntry entry) {
        List<String> result = new ArrayList<String>();
        for (Email email : entry.getEmailAddresses()) {
            result.add(email.getAddress());
        }
        return result;
    }

}
