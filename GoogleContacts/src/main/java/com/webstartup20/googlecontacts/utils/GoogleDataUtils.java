package com.webstartup20.googlecontacts.utils;

import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.extensions.AdditionalName;
import com.google.gdata.data.extensions.Name;
import com.google.gdata.data.extensions.NamePrefix;
import com.google.gdata.data.extensions.NameSuffix;
import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.struct.IContact;
import com.webstartup20.struct.Contact;

import java.util.*;

import static com.webstartup20.googlecontacts.utils.ContactEntryUtils.*;
import static com.webstartup20.googlecontacts.utils.NameUtils.*;

public class GoogleDataUtils{

    public static void putNameDataToMap(Map<String, String> data, String fullName,
                                        String namePrefix,
                                        String givenName,
                                        String additionalName,
                                        String familyName,
                                        String nameSuffix) {
        data.put(ResourcesConstants.Google.HEADER[0], fullName);
        data.put(ResourcesConstants.Google.HEADER[1], namePrefix);
        data.put(ResourcesConstants.Google.HEADER[2], givenName);
        data.put(ResourcesConstants.Google.HEADER[3], additionalName);
        data.put(ResourcesConstants.Google.HEADER[4], familyName);
        data.put(ResourcesConstants.Google.HEADER[5], nameSuffix);
    }

    public static void putNameDataToMap(Map<String, String> data) {
        putNameDataToMap(data, "", "", "", "", "", "");
    }

    public static void putExternalDataToMap(Map<String, String> data,
                                            String imAddress,
                                            String group,
                                            String extendedeProperty,
                                            String photoLink,
                                            String eTag) {
        data.put(ResourcesConstants.Google.HEADER[7], imAddress);
        data.put(ResourcesConstants.Google.HEADER[8], group);
        data.put(ResourcesConstants.Google.HEADER[9], extendedeProperty);
        data.put(ResourcesConstants.Google.HEADER[10], photoLink);
        data.put(ResourcesConstants.Google.HEADER[11], eTag);
    }

    public static void putExternalDataToMap(Map<String, String> data){
        putExternalDataToMap(data,  "", "", "", "", "");
    }

    public static List<Map<String, String>> getContactList(ContactFeed resultFeed) {
        List<Map<String, String>> contacts = new ArrayList<>();
        for (ContactEntry entry : resultFeed.getEntries()) {
            Map<String, String> contact = new LinkedHashMap<>();

            Name name = entry.getName();
            if (name != null) {
                putNameDataToMap(contact,
                        name.getFullName()!=null?name.getFullName().getValue():"",
                        getNamePrefix(name),
                        name.getGivenName()!=null?name.getGivenName().getValue():"",
                        getAdditionalName(name),
                        name.getFamilyName()!=null?name.getFamilyName().getValue():"",
                        getNameSuffix(name));
            } else {
                putNameDataToMap(contact);
            }

            putExternalDataToMap(contact,
                    getImAddresses(entry),
                    getGroups(entry),
                    getExtendedProperties(entry),
                    getPhotoLink(entry),
                    getEtag(entry)
            );

            List<String> emails = getEmails(entry);
            for (int i = 0; i < emails.size(); i++) {
                if(i == 0){
                    contact.put(ResourcesConstants.Google.HEADER[6], emails.get(i));
                }else{
                    Map<String, String> contactsBuf = new HashMap<>();
                    putNameDataToMap(contactsBuf);
                    putExternalDataToMap(contactsBuf);
                    contactsBuf.put(ResourcesConstants.Google.HEADER[6], emails.get(i));
                    contacts.add(contactsBuf);
                }
            }

            contacts.add(contact);
        }
        return contacts;
    }

    private static String getNamePrefix(Name name){
        NamePrefix namePrefix = name.getNamePrefix();
        return (namePrefix != null) ? namePrefix.getValue() : "";
    }

    private static String getAdditionalName(Name name){
        AdditionalName additionalName = name.getAdditionalName();
        return (additionalName != null) ? additionalName.getValue() : "";
    }

    private static String getNameSuffix(Name name){
        NameSuffix nameSuffix = name.getNameSuffix();
        return (nameSuffix != null) ? nameSuffix.getValue() : "";
    }

    public static List<IContact> convetResultFeedToList(ContactFeed resultFeed){
        List<IContact> contacts = new ArrayList<>();
        for (ContactEntry entry : resultFeed.getEntries()) {
            Contact contact = new Contact();
            Name name = entry.getName();
            if (name != null) {
                contact.setFirstName(getGivenName(name));
                contact.setLastName(getFamilyName(name));
            }else{
                contact.setFirstName("");
                contact.setLastName("");
            }
            contact.setEmails(getEmails(entry));
            contacts.add(contact);
        }
        return contacts;
    }


}
