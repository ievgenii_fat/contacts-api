package com.webstartup20.utils.field_map;


import com.webstartup20.constants.ResourcesConstants;

/**
 * Created by Sergiy on 06-Oct-14.
 */
public class FieldsMapper {


    private MailChimpFieldName mailChimpFieldName;
    private GoogleFieldName googleFieldName;
    private GeneralFieldName generalFieldName;
    private CsvFieldName csvFieldName;
    private boolean skip;

    public FieldsMapper(){}

    public FieldsMapper(MailChimpFieldName mailChimpFieldName,GoogleFieldName googleFieldName,GeneralFieldName generalFieldName, boolean skip){
        this.googleFieldName = googleFieldName;
        this.mailChimpFieldName = mailChimpFieldName;
        this.generalFieldName = generalFieldName;
        this.skip = skip;
    }

    public void setGoogleFieldName(GoogleFieldName googleFieldName) {
        this.googleFieldName = googleFieldName;
    }

    public void setMailChimpFieldName(MailChimpFieldName mailChimpFieldName) {
        this.mailChimpFieldName = mailChimpFieldName;
    }

    public void setGeneralFieldName(GeneralFieldName generalFieldName) {
        this.generalFieldName = generalFieldName;
    }

    public void setCsvFieldName(CsvFieldName csvFieldName) {
        this.csvFieldName = csvFieldName;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public FieldName getFieldNameFor(String className){
        switch (className){
            case ResourcesConstants.General.NAME :
                return getGeneralFieldName();
            case ResourcesConstants.Google.NAME:
                return  getGoogleFieldName();
            case ResourcesConstants.Mailchimp.NAME :
                return  getMailChimpFieldName();
            case ResourcesConstants.CsvDownload.NAME :
                return  getCsvFieldName();
            case ResourcesConstants.CsvUpload.NAME :
                return  getCsvFieldName();
            default:
                return  getGeneralFieldName();
        }
    }

    public GoogleFieldName getGoogleFieldName() {
        return googleFieldName;
    }

    public MailChimpFieldName getMailChimpFieldName() {
        return mailChimpFieldName;
    }

    public GeneralFieldName getGeneralFieldName(){return generalFieldName;}
    public CsvFieldName getCsvFieldName(){return csvFieldName;}

    public boolean isSkip() {
        return skip;
    }

    @Override
    public String toString() {
        return "FieldsMapper{" +
                "mailChimpFieldName=" + mailChimpFieldName +
                ", googleFieldName=" + googleFieldName +
                ", generalFieldName=" + generalFieldName +
                ", csvFieldName=" + csvFieldName +
                ", skip=" + skip +
                '}';
    }

    public static abstract class FieldName{
        protected String name;

        public FieldName(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
        @Override
        public String toString() {
            return "FieldName: "+name;
        }
    }

    public static class MailChimpFieldName extends FieldName{
        public MailChimpFieldName(String name){
            super(name);
        }
        @Override
        public String toString() {
            return "MailChimpFieldName: "+name;
        }
    }

    public static class GoogleFieldName extends FieldName{
        public GoogleFieldName(String name){
            super(name);
        }
        @Override
        public String toString() {
            return "GoogleFieldName: "+name;
        }
    }

    public static class GeneralFieldName extends FieldName{
        public GeneralFieldName(String name){
            super(name);
        }
        @Override
        public String toString() {
            return "GeneralFieldName: "+name;
        }
    }

    public static class CsvFieldName extends FieldName{
        public CsvFieldName(String name){
            super(name);
        }
        @Override
        public String toString() {
            return "GeneralFieldName: "+name;
        }
    }


}
