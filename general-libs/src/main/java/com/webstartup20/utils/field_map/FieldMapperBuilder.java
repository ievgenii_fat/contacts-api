package com.webstartup20.utils.field_map;


import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.entity.pojo.MappingPojo;
import com.webstartup20.io.MessagesPrinter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: segn1014
 * Date: 12/22/14
 * Time: 4:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class FieldMapperBuilder {

    public static final String YES = "yes";
    public static final String NO = "no";

    private List<FieldsMapper> fieldsMappers;
    private MappingPojo[] mapping;
    private Map<Integer, String> controllers;

    public FieldMapperBuilder(MappingPojo[] mapping, Map<Integer, String> controllers) {
        fieldsMappers = new ArrayList<>();
        this.mapping = mapping;
        this.controllers = controllers;
        processConnector();
    }

    private void processConnector() {
        for (MappingPojo mapping : this.mapping) {
            FieldsMapper fieldsMapper = new FieldsMapper();

            String sourceType = getAccountTypeById(mapping.getSourceIcon());
            String targetType = getAccountTypeById(mapping.getTargetIcon());

            if (sourceType == null || targetType == null) {
                continue;
            }

            boolean succeed = setFieldToMapper(sourceType, mapping.getSourceField(), fieldsMapper);
            succeed &= setFieldToMapper(targetType, mapping.getTargetField(), fieldsMapper);

            if (succeed) {
                fieldsMapper.setSkip(determineIsSkip(mapping.getFieldCheckOptions()));
                fieldsMappers.add(fieldsMapper);
            }
        }
    }

    private boolean determineIsSkip(String skip) {
        if (skip == null) {
            return false;
        }

        switch (skip) {
            case YES:
                return true;
            case NO:
                return false;
            default:
                return false;
        }
    }

    private String getAccountTypeById(int id) {
        return controllers.get(id);
    }

    public Map<Integer, String> getControllersIdNamePair(){
        return controllers;
    }

    private boolean setFieldToMapper(String type, String filedName, FieldsMapper fieldsMapper) {
        switch (type) {
            case ResourcesConstants.Google.NAME:
                if (isValidField(filedName, ResourcesConstants.Google.HEADER)) {
                    fieldsMapper.setGoogleFieldName(new FieldsMapper.GoogleFieldName(filedName));
                    return true;
                }
            case ResourcesConstants.Mailchimp.NAME:
                if (isValidField(filedName, ResourcesConstants.Mailchimp.HEADER)) {
                    fieldsMapper.setMailChimpFieldName(new FieldsMapper.MailChimpFieldName(filedName));
                    return true;
                }
            case ResourcesConstants.CsvDownload.NAME:
                if (isValidField(filedName, ResourcesConstants.CsvDownload.HEADER)) {
                    fieldsMapper.setCsvFieldName(new FieldsMapper.CsvFieldName(filedName));
                    return true;
                }
            case ResourcesConstants.CsvUpload.NAME:
                if (isValidField(filedName, ResourcesConstants.CsvUpload.HEADER)) {
                    fieldsMapper.setCsvFieldName(new FieldsMapper.CsvFieldName(filedName));
                    return true;
                }
            case ResourcesConstants.General.NAME:
                if (isValidField(filedName, ResourcesConstants.General.HEADER)) {
                    fieldsMapper.setGeneralFieldName(new FieldsMapper.GeneralFieldName(filedName));
                    return true;
                }
        }
        return false;
    }

    private boolean isValidField(String field, String[] fields) {
        for (String value : fields) {
            if (field.equalsIgnoreCase(value)) {
                return true;
            }
        }
        MessagesPrinter.getInstance().printErrorMessage(this.getClass(), "Invalid field name '" + field + "'");
        return false;
    }

    public List<FieldsMapper> getFieldsMappers() {
        return fieldsMappers;
    }

    @Override
    public String toString() {
        return "FieldMapperBuilder{" +
                "fieldsMappers=" + fieldsMappers +
                '}';
    }
}
