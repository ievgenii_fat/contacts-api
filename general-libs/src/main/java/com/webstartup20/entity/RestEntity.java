package com.webstartup20.entity;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

/**
 * Created by FAT on 14.12.2014.
 */
public interface RestEntity extends Serializable {
    public Map<String, Object> collectData();
}
