package com.webstartup20.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sergiy on 09-Jan-15.
 */
public class RestEntitiesContainer implements RestEntity {

    private Map<String, Object> entities = new HashMap<>();

    public Map<String, Object> getEntities() {
        return entities;
    }

    public void setEntities(Map<String, Object> entities) {
        this.entities = entities;
    }

    public void addEntity(String name, Object entity) {
        entities.put(name, entity);
    }

    @Override
    public String toString() {
        return "RestEntitiesContainer{" +
                "entities=" + entities +
                '}';
    }

    @Override
    public Map<String, Object> collectData() {
        Map<String, Object> data = new HashMap<>();
        for (String key : entities.keySet()) {
            data.put(key, entities.get(key));
        }
        return data;
    }
}
