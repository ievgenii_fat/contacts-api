package com.webstartup20.entity;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sergiy on 09-Jan-15.
 */
@JsonIgnoreProperties({"arrayName"})
public class RestEntitiesArray implements RestEntity {

    private String arrayName;

    public RestEntitiesArray(String arrayName) {
        this.arrayName = arrayName;
    }

    private List<Object> entities = new ArrayList<>();


    @JsonIgnore
    public List<Object> getEntities() {
        return entities;
    }

    @JsonAnyGetter
    public Map<String,Object> otherFields() {
        Map<String, Object> data = new HashMap<>();
        data.put(arrayName, entities);
        return data;
    }

    public void setEntities(List<Object> entities) {
        this.entities = entities;
    }

    public void addEntity(Map<String, Object> data) {
        SimpleEntity entity = new SimpleEntity();
        entity.addData(data);
        entities.add(entity);
    }

    public void addEntity(String key, Object value) {
        SimpleEntity entity = new SimpleEntity();
        entity.addData(key, value);
        entities.add(entity);
    }

    public void addEntity(RestEntity entity) {
        entities.add(entity);
    }

    @Override
    public String toString() {
        return "RestEntitiesArray{" +
                "entities=" + entities +
                '}';
    }

    @Override
    public Map<String, Object> collectData() {
        return otherFields();
    }
}
