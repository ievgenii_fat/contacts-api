package com.webstartup20.entity.pojo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Arrays;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectorPOJO implements WorkflowEntity{

    @JsonProperty("mapping")
	private MappingPojo[] mapping;
    @JsonProperty("id")
    private int id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("outcomingConnections")
    private String[] incomingConnections;
    @JsonProperty("incomingConnections")
    private String[] outcomingConnections;
    @JsonProperty("mergeParams")
    private String[] mergeParams;

    public void setMapping(MappingPojo[] mapping){
        this.mapping = mapping;
    }

    public MappingPojo[] getMapping(){
        return mapping;
    }

    public int getId(){
        return id;
    }

    public String getType(){
        return type;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type){
        this.type = type;
    }

    public String[] getMergeParams() {
        return mergeParams;
    }

    public void setMergeParams(String[] mergeParams) {
        this.mergeParams = mergeParams;
    }

    public String[] getIncomingConnections() {
        return incomingConnections;
    }

    public void setIncomingConnections(String[] incomingConnections) {
        this.incomingConnections = incomingConnections;
    }

    public String[] getOutcomingConnections() {
        return outcomingConnections;
    }

    public void setOutcomingConnections(String[] outcomingConnections) {
        this.outcomingConnections = outcomingConnections;
    }

    @Override
    public String toString() {
        return "ConnectorPOJO{" +
                "mapping=" + Arrays.toString(mapping) +
                ", id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
