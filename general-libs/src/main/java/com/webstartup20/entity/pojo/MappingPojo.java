package com.webstartup20.entity.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Sergiy on 11-Jan-15.
 */
public class MappingPojo {

    @JsonProperty("sourceIcon")
    private int sourceIcon;
    @JsonProperty("targetIcon")
    private int targetIcon;
    @JsonProperty("sourceField")
    private String sourceField;
    @JsonProperty("targetField")
    private String targetField;
    @JsonProperty("fieldCheckOptions")
    private String fieldCheckOptions;

    public int getSourceIcon() {
        return sourceIcon;
    }
    public void setSourceIcon(int sourceIcon) {
        this.sourceIcon = sourceIcon;
    }
    public int getTargetIcon() {
        return targetIcon;
    }
    public void setTargetIcon(int targetIcon) {
        this.targetIcon = targetIcon;
    }
    public String getSourceField() {
        return sourceField;
    }
    public void setSourceField(String sourceField) {
        this.sourceField = sourceField;
    }
    public String getTargetField() {
        return targetField;
    }
    public void setTargetField(String targetField) {
        this.targetField = targetField;
    }
    public String getFieldCheckOptions() {
        return fieldCheckOptions;
    }
    public void setFieldCheckOptions(String fieldCheckOptions) {
        this.fieldCheckOptions = fieldCheckOptions;
    }

    @Override
    public String toString() {
        return "MappingPojo{" +
                "sourceIcon=" + sourceIcon +
                ", targetIcon=" + targetIcon +
                ", sourceField='" + sourceField + '\'' +
                ", targetField='" + targetField + '\'' +
                '}';
    }
}