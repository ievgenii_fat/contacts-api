package com.webstartup20.entity.pojo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Arrays;

/**
 * Created by Sergiy on 18-Dec-14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountPojo implements WorkflowEntity{

    @JsonProperty("id")
    private int id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("loginInfo")
    private LoginInfoPojo loginInfo;

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LoginInfoPojo getLoginInfo() {
        return loginInfo;
    }

    public void setLoginInfo(LoginInfoPojo loginInfo) {
        this.loginInfo = loginInfo;
    }


    @Override
    public String toString() {
        return "AccountPojo{" +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", loginInfo=" + loginInfo +
                '}';
    }
}
