package com.webstartup20.entity.pojo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by FAT on 21.12.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginInfoPojo {
    @JsonProperty("API")
    private String API;
    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;
    @JsonProperty("listId")
    private String listId;
    @JsonProperty("fileName")
    private String fileName;
    @JsonProperty("token")
    private String token;

    public String getAPI() {
        return API;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getListId() {
        return listId;
    }

    public void setAPI(String API) {
        this.API = API;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginInfoPojo{" +
                "API='" + API + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", listId='" + listId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
