package com.webstartup20.entity.pojo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowPOJO {

    @JsonProperty("connectors")
    private ConnectorPOJO[] connectors;
    @JsonProperty("accounts")
    private AccountPojo[] accounts;

    public void setConnectors(ConnectorPOJO[] connectors) {
        this.connectors = connectors;
    }

    public ConnectorPOJO[] getConnectors() {
        return connectors;
    }

    public AccountPojo[] getAccounts() {
        return accounts;
    }

    public void setAccounts(AccountPojo[] accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "WorkflowPOJO{" +
                "connectors=" + Arrays.toString(connectors) +
                ", accounts=" + Arrays.toString(accounts) +
                '}';
    }
}
