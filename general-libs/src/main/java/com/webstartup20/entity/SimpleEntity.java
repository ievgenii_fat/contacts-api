package com.webstartup20.entity;

import com.webstartup20.dto.PersistentResponse;
import org.codehaus.jackson.annotate.JsonAnyGetter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sergiy on 09-Jan-15.
 */
public class SimpleEntity implements PersistentResponse {

    private Map<String, Object> data = new HashMap<>();

//    public Map<String, Object> getData() {
//        return data;
//    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public void addData(String key, Object value) {
        data.put(key, value);
    }
    public void addData(Map<String, Object> data) {
        for (String key : data.keySet()) {
            this.data.put(key, data.get(key));
        }
    }


    @JsonAnyGetter
    public Map<String,Object> otherFields() {
        return data;
    }

    @Override
    public Map<String, Object> collectData() {
        return new HashMap<>(data);
    }

    @Override
    public String toString() {
        return "SimpleEntity{" +
                "data=" + data +
                '}';
    }

}
