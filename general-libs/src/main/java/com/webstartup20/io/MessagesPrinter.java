package com.webstartup20.io;

/**
 * Created by Sergiy on 02-Oct-14.
 */
public class MessagesPrinter {

    private static int ID = 0;

    private static MessagesPrinter messagesPrinter = new MessagesPrinter();


    private MessagesPrinter(){}

    public static MessagesPrinter getInstance(){
        return messagesPrinter;
    }

    public synchronized void printMessage(Class objectClass, String message){
        System.out.format("{%s}: %s\n",objectClass.getName(), message);
    }

    public synchronized int printErrorMessage(Class objectClass, String message){
        System.err.format("ERROR[ID: %s]: {%s}  %s\n", ++ID, objectClass.getName(), message);
        return ID;
    }
}
