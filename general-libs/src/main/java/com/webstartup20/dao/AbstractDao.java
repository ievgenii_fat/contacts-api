package com.webstartup20.dao;


import com.webstartup20.io.MessagesPrinter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractDao {
    protected Connection connection;

    public AbstractDao(){
        connection = ConnectionFactory.getConnection();
    }

    public abstract String getTableName();

    public void closeConnection(){
        close(connection);
    }

    protected void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                MessagesPrinter.getInstance().printMessage(getClass(), "Error closing Mysql DB connection");
            }
        }
    }

    protected void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                MessagesPrinter.getInstance().printMessage(getClass(), "Error closing Mysql DB statement");
            }
        }
    }

    public void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                MessagesPrinter.getInstance().printMessage(getClass(), "Error closing Mysql DB result set");
            }
        }
    }

    protected void closeAll(Connection connection, Statement statement, ResultSet resultSet) {
        close(resultSet);
        close(statement);
        close(connection);
    }
}
