package com.webstartup20.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by FAT on 24.01.2015.
 */
public class SimpleEntityDao extends AbstractDao{

    private static final String MESSAGE = "message";
    private static final String IMPORTED = "imported";
    private static final String REJECTED = "rejected";


    public SimpleEntityDao(){
        super();
    }


    @Override
    public String getTableName() {
        return "events";
    }

    private void writeToDb(String message, int imported, int rejected){
        try(Statement statement = connection.createStatement()){
            int valuesNumber = 3;
            String columns = String.format(createPattern("`",valuesNumber), MESSAGE, IMPORTED, REJECTED);
            String values = String.format(createPattern("'",valuesNumber), message, imported, rejected);
            String query = String.format("INSERT INTO %s %s VALUES %s;", getTableName(), columns, values);
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String createPattern(String sign, int valuesNumber){
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        for (int i = 0; i < valuesNumber; i++) {
            if(i != 0){
                builder.append(",");
            }
            builder.append(sign);
            builder.append("%s");
            builder.append(sign);
        }
        builder.append(")");
        return builder.toString();
    }

    public void writeToDb(SimpleLog logData){
        writeToDb(logData.getMessage(), logData.getImported(), logData.getRejected());
    }

    public static class SimpleLog{
        private int imported;
        private int rejected;
        private String message;

        public int getImported() {
            return imported;
        }

        public void setImported(int imported) {
            this.imported = imported;
        }

        public int getRejected() {
            return rejected;
        }

        public void setRejected(int rejected) {
            this.rejected = rejected;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
