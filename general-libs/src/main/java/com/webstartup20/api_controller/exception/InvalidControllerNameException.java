package com.webstartup20.api_controller.exception;

/**
 * Created by Sergiy on 13-Jan-15.
 */
public class InvalidControllerNameException extends Exception {

    public InvalidControllerNameException() {
        super();
    }

    public InvalidControllerNameException(String s) {
        super(s);
    }

}
