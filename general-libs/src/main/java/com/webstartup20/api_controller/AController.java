package com.webstartup20.api_controller;


import com.webstartup20.api_controller.exception.InvalidControllerNameException;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.struct.IContact;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.List;
import java.util.Map;

/**
 * Created by Sergiy on 20-Dec-14.
 */
public abstract class AController {

    private int id;

    public static final String IMPORTED = "imported";
    public static final String REJECTED = "rejected";
    public static final String MESSAGE = "message";

    public AController(int id){
        this.id = id;
    }

    public abstract RestEntity importContacts(List<Map<String, String>> contacts, FieldMapperBuilder fieldMapperBuilder);
    public abstract List<Map<String, String>> exportContacts();
    public abstract String getControllerName();

    protected String determineExportController(FieldMapperBuilder fieldMapperBuilder) throws InvalidControllerNameException {
        Map<Integer, String> idNamePair = fieldMapperBuilder.getControllersIdNamePair();
        for (Integer id : idNamePair.keySet()) {
            String controllerName = idNamePair.get(id);
            if(!controllerName.equalsIgnoreCase(getControllerName())){
                return controllerName;
            }
        }
        throw new InvalidControllerNameException();
    }

    protected RestEntity generateResponse(String message, int imported, int rejected){
        SimpleEntity simpleEntity = new SimpleEntity();

        simpleEntity.addData(MESSAGE, message);
        simpleEntity.addData(IMPORTED, imported);
        simpleEntity.addData(REJECTED, rejected);

        return simpleEntity;
    }

    public int getId(){
        return id;
    }

    @Override
    public String toString() {
        return "AController{" +
                "id=" + id + ", "+
                "name=" + getControllerName() +
                '}';
    }
}
