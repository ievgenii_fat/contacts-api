package com.webstartup20.constants;

import com.webstartup20.io.MessagesPrinter;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: segn1014
 * Date: 1/14/15
 * Time: 4:47 PM
 * To change this template use File | Settings | Fi le Templates.
 */
public abstract class PathConstants {

    public PathConstants(){}

    public static final String REST_DIR_NAME = "./rest/";

    public static final String DOWNLOAD_DIR_NAME = "downloads";
    public static final String UPLOAD_DIR_NAME = "uploads";

    public static final String DOWNLOAD_DIR_PATH = createDir(DOWNLOAD_DIR_NAME);
    public static final String UPLOAD_DIR_PATH = createDir(UPLOAD_DIR_NAME);

    private static String createDir(String dirName){
        String newDirName = REST_DIR_NAME + dirName+"/";
        File newDirFile = new File(newDirName);
        if(!newDirFile.exists()){
            if(newDirFile.mkdirs()){
                String success = String.format("'%s' directory was created", newDirName);
                MessagesPrinter.getInstance().printMessage(PathConstants.class, success);
            }else {
                String errorMessage = String.format("Error during creation of '%s'. Create it manually", newDirName);
                MessagesPrinter.getInstance().printErrorMessage(PathConstants.class, errorMessage);
            }
        }
        return newDirName;
    }
}
