package com.webstartup20.constants;

/**
 * Created by Sergiy on 11-Jan-15.
 */
public final class ResourcesConstants {
    private ResourcesConstants(){}

    public static final int FIRST_NAME_CONSTANT = 1;
    public static final int LAST_NAME_CONSTANT = 2;
    public static final int EMAIL_CONSTANT = 3;

    public static final int[] MAIN_FIELDS_SET = {FIRST_NAME_CONSTANT, LAST_NAME_CONSTANT, EMAIL_CONSTANT};

    public static class Google{
        public static final String NAME = "google";

        public static final String[] HEADER = {
                "Full Name",
                "Name prefix",
                "Given name",
                "Additional name",
                "Family name",
                "Name suffix",
                "Emails",
                "IM addresses",
                "Groups",
                "Extended Properties",
                "Photo Link",
                "Etag"
        };
    }

    public static class Mailchimp{
        public static final String NAME = "mailchimp";
        public static final String[] HEADER = {
                "first name",
                "last name",
                "email",
        };
    }

    public static class CsvDownload{
        public static final String[] HEADER = Mailchimp.HEADER;
        public static final String NAME = "excel-DOWN";
    }

    public static class CsvUpload{
        public static final String[] HEADER = Mailchimp.HEADER;
        public static final String NAME = "excel-UP";
    }

    public static class General{
        public static final String NAME = "general";

        public static final String[] HEADER = Mailchimp.HEADER;

    }

}
