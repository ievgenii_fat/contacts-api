package com.webstartup20.dto;

/**
 * Created by FAT on 24.01.2015.
 */
public enum EventType {
    DF("mailchimp"),
    DM("google");

    private String name;

    EventType(String name) {
        this.name = name;
    }
}
