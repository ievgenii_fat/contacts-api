package com.webstartup20.struct;

        import java.util.List;

/**
 * Created by Sergiy on 21-Dec-14.
 */
public interface IContact {
    public List<String> getEmails();
    public String getFirstName();
    public String getLastName();
}
