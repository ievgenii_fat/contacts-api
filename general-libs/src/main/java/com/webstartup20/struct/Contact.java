package com.webstartup20.struct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergiy on 07-Oct-14.
 */
public class Contact implements IContact {
    private String firstName = " ";
    private String lastName = " ";
    private List<String> emails;

    private boolean validData = false;

    public Contact(){
        emails = new ArrayList<>();
    }

    public List<String> getEmails() {
        return emails;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void addEmail(String email){
        emails.add(email);
    }


    public boolean isValidData() {
        return validData;
    }

    public void setValidData(boolean validData) {
        this.validData = validData;
    }

    @Override
    public String toString() {
        String emailsString = "";
        for (String email: emails){
            emailsString += "["+email+"],";
        }
        return String.format("[First name: %s, Last name: %s, Emails: %s]", firstName, lastName, emailsString);
    }
}
