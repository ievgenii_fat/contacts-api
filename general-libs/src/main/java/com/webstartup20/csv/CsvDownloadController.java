package com.webstartup20.csv;

import com.webstartup20.constants.PathConstants;
import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.struct.FileUtils;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Sergiy on 11-Jan-15.
 */
public class CsvDownloadController extends CsvController {

    private static int fileCounter = 0;

    public CsvDownloadController(int id) {
        super(id);
    }

    @Override
    public String getControllerName() {
        return ResourcesConstants.CsvDownload.NAME;
    }

    @Override
    public List<Map<String, String>> exportContacts() {
        return Collections.emptyList();
    }

    @Override
    public RestEntity importContacts(List<Map<String, String>> contacts, FieldMapperBuilder fieldMapperBuilder) {

        SimpleEntity entity = new SimpleEntity();

        if(contacts.size() <= 0){
            entity.addData(MESSAGE, "no data");
            return entity;
        }

        List<String> rowContacts = CsvUtil.generateListForRecordToFile(contacts);
        String fileName = String.format("%s_%s%s", FileUtils.getFileNameTimeStamp(), getFileCounter(), CsvUtil.FORMAT);
        FileUtils.writeToFile(rowContacts, PathConstants.DOWNLOAD_DIR_PATH + fileName);

        entity.addData("file", fileName);
        entity.addData(generateResponse("success",contacts.size(), 0).collectData());
        return entity;
    }

    private synchronized static int getFileCounter(){
        return fileCounter++;
    }

}
