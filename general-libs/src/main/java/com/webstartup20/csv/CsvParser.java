package com.webstartup20.csv;

import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.csv.CsvUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvParser {

    private static final String EMPTY = "";

    protected Map<Integer, String> header;
    protected List<Map<String, String>> data;

    public CsvParser(String filePath){
        this(new File(filePath));
    }

    public CsvParser(File file){
        data = new ArrayList<>();
        header = new HashMap<>();
        readData(file);
    }


    protected void readData(File file){

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            processHeader(br.readLine());
            while ((line = br.readLine()) != null) {

                if(isEmptyRow(line)){
                    continue;
                }
                data.add(parseRow(line));
            }
        }catch (FileNotFoundException e){
            String errorMes = String.format("File '%s' not found", file.getAbsolutePath());
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(),errorMes);
        }catch (IOException e){
            String errorMes = String.format("Error during reading file '%s'", file.getAbsolutePath());
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(),errorMes);
        }
    }

    protected void processHeader(String row){
        String[] headerArray = CsvUtil.parseCsvRow(row);
        for (int i = 0; i < headerArray.length; i++) {
            if(headerArray[i].replace(CsvUtil.DELIM,"").length() <= 0){
                header.put(i, "");
            }else{
                header.put(i, headerArray[i]);
            }
        }
    }

    protected boolean isEmptyRow(String row){
        String buff = row.replace(CsvUtil.DELIM, "");
        buff = buff.replace(" ", "");
        return buff.length() == 0;
    }

    protected Map<String, String> parseRow(String row){
        String[] splittedRow = CsvUtil.parseCsvRow(row);

        Map<String, String> data = new HashMap<>();

        for (int i = 0; i < splittedRow.length; i++) {

            if(header.get(i).equals(EMPTY)){
                continue;
            }
            data.put(header.get(i), splittedRow[i]);
        }

        return data;
    }

    public List<Map<String, String>> getData() {
        return data;
    }
}
