package com.webstartup20.csv;

import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.struct.FileUtils;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Sergiy on 11-Jan-15.
 */
public class CsvUploadController extends CsvController {

    public CsvUploadController(int id, String filePath) {
        super(id);
        this.filePath = filePath;
    }

    @Override
    public String getControllerName() {
        return ResourcesConstants.CsvUpload.NAME;
    }

    @Override
    public RestEntity importContacts(List<Map<String, String>> contacts, FieldMapperBuilder fieldMapperBuilder) {
        return new RestEntity() {

            @Override
            public Map<String, Object> collectData() {
                HashMap<String, Object> data = new HashMap<>();
                data.put("error",error);
                return data;
            }

            private String error = String.format("Cann't import to '%s'", getControllerName());
            public String getError() {
                return error;
            }
            public void setError(String error) {
                this.error = error;
            }
        };
    }

    @Override
    public List<Map<String, String>> exportContacts() {
        MessagesPrinter.getInstance().printMessage(this.getClass(), String.format("Reading file '%s'",filePath));
        return new CsvParser(filePath).getData();
    }

}
