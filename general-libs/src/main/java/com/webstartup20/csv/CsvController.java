package com.webstartup20.csv;

import com.webstartup20.api_controller.AController;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.struct.FileUtils;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.*;

/**
 * Created by Sergiy on 03-Jan-15.
 */
public abstract class CsvController extends AController {

    protected String filePath;

    public CsvController(int id) {
        super(id);
    }

    @Override
    public abstract RestEntity importContacts(List<Map<String, String>> contacts, FieldMapperBuilder fieldMapperBuilder);

    @Override
    public abstract List<Map<String, String>> exportContacts();

    public static List<Map<String, String>> convertToListOfMaps(List<String[]> rows){
        List<Map<String, String>> result = new ArrayList<>();

        String[] header = getHeaderFromList(rows);

        for (int i = 1; i < rows.size(); i++) {
            Map<String, String> map = new LinkedHashMap<>();
            String[] row = rows.get(i);
            for (int j = 0; j < row.length; j++) {
                map.put(header[j], row[j]);
            }
            result.add(map);
        }

        return result;
    }

    private static String[] getHeaderFromList(List<String[]> rows){
        for (String[] row : rows) {
            return row;
        }
        return new String[]{};
    }

    public static String[][] createDataArray(Collection<Map<String, String>> contacts){
        String[][] data = new String[contacts.size()][];
        int i = 0;
        for (Map<String, String> contact : contacts) {
            data[i] = new String[contact.size()];
            int j = 0;
            for (String value : contact.keySet()) {
                data[i][j] = contact.get(value);
                j++;
            }
            i++;
        }
        return data;
    }

    public static String[] createHeader(Collection<Map<String, String>> contacts){
        for (Map<String, String> contact : contacts) {
            return  contact.keySet().toArray(new String[]{});
        }
        return new String[]{};
    }

}
