package com.webstartup20.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Sergiy on 10-Oct-14.
 */
public class CsvUtil {

    public static final String FORMAT = ".csv";
    public static final String DELIM = ",";

    public static List<String> convertToCsvRowFormat(List<String[]> rows){
        List<String> result = new ArrayList<>();
        for (String[] row:rows){
            result.add(arrayToCsvRow(row));
        }
        return result;
    }


    public static String arrayToCsvRow(String[] row){
        String buffer = "";
        for (String element: row){
            buffer += (element.contains(DELIM)) ? "\""+element+"\"" : element;
            buffer += DELIM;
        }
        return buffer;
    }

    public static List<String> generateListForRecordToFile(List<Map<String, String>> contacts) {
        List<String> data = new ArrayList<>();
        data.add(CsvUtil.arrayToCsvRow(CsvController.createHeader(contacts)));
        data.addAll(convertToStringList(contacts));
        return data;
    }


    private static List<String> convertToStringList(List<Map<String, String>> contacts) {
        List<String> output = new ArrayList<>();
        for (Map<String, String> map : contacts) {
            output.add(arrayToCsvRow(map.values().toArray(new String[]{})));
        }
        return output;
    }


    public static String[] parseCsvRow(String str) {
        try {
            CSVParser csvParser = CSVParser.parse(str, CSVFormat.DEFAULT);
            for (CSVRecord csvRecord : csvParser.getRecords()) {
                String[] result = new String[csvRecord.size()];
                int i = 0;
                for (String elem : csvRecord) {
                    result[i] = elem;
                    i++;
                }
                return result;
            }
        } catch (IOException e) {
            System.err.println("CSV data parse error. Please check data of input files");
            e.printStackTrace();
        }

        return str.split(CsvUtil.DELIM);
    }

}
