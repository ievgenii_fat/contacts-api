package com.webstartup20.rest.entity.connection;

import com.webstartup20.api_controller.AController;
import com.webstartup20.dao.SimpleEntityDao;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.pojo.ConnectorPOJO;
import com.webstartup20.struct.FileUtils;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sergiy on 21-Dec-14.
 */
public abstract class AConnection {

    protected ConnectorPOJO connectorPOJO;
    protected SimpleEntityDao entityDao;

    public AConnection(ConnectorPOJO connectorPOJO) {
        this.connectorPOJO = connectorPOJO;
        entityDao = new SimpleEntityDao();
    }

    public static final String DF_CONNECTION = "connect_DF";
    public static final String DM_CONNECTION = "connect_DM";

    public abstract RestEntity connect(List<AController> exportControllers, List<AController> importControllers);

    protected FieldMapperBuilder getFieldMapperBuilder(AController exportController, AController importController) {
        Map<Integer, String> controllers = generateIdNameMapForControllers(exportController, importController);
        return new FieldMapperBuilder(connectorPOJO.getMapping(), controllers);
    }

    /**
     *
     * @param controllers
     * @return
     */
    protected Map<Integer, String> generateIdNameMapForControllers(AController... controllers){
        Map<Integer, String> result = new HashMap<>();

        for (AController controller : controllers) {
            result.put(controller.getId(), controller.getControllerName());
        }

        return result;
    }

    protected void writeImportLog(String controller, Map<String, Object> data){

        int imported = parseInt(data.get(AController.IMPORTED));
        int rejected = parseInt(data.get(AController.REJECTED));

        String message = generateMessageHead();
        message += controller;

        SimpleEntityDao.SimpleLog simpleLog1 = new SimpleEntityDao.SimpleLog();
        simpleLog1.setMessage(message);
        simpleLog1.setImported(imported);
        simpleLog1.setRejected(rejected);

        entityDao.writeToDb(simpleLog1);
    }

    private int parseInt(Object number){
        return (number == null) ? 0 : Integer.parseInt(number.toString());
    }

    protected String generateMessageHead(){
        String message = String.format("[%s]", this.getClass().getSimpleName());
        message += String.format("{%s}", FileUtils.getTimeStamp("yyyy:MM:dd:hh:mm:ss"));
        return message;
    }
}
