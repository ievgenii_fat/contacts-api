package com.webstartup20.rest.entity.connection;

import com.webstartup20.api_controller.AController;
import com.webstartup20.csv.CsvController;
import com.webstartup20.dao.SimpleEntityDao;
import com.webstartup20.entity.RestEntitiesArray;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.entity.pojo.ConnectorPOJO;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.merger.Merger;

import java.util.*;

/**
 * Created by Sergiy on 04-Jan-15.
 */
public class DmConnection extends AConnection {

    private SimpleEntityDao.SimpleLog simpleLog;

    public DmConnection(ConnectorPOJO connectorPOJO) {
        super(connectorPOJO);
        simpleLog = new SimpleEntityDao.SimpleLog();
    }

    private List<Map<String, String>> contactsDuplicates = new ArrayList<>();

    @Override
    public RestEntity connect(List<AController> exportControllers, List<AController> importControllers) {

        SimpleEntity restEntities = new SimpleEntity();

        restEntities.addData("id", new Integer(connectorPOJO.getId()));

        if (exportControllers.size() > 0) {

            List<Map<String, String>> contacts = mergeContactsFromControllers(exportControllers);
            restEntities.addData(importDataToControllers(importControllers, contacts).collectData());

            MessagesPrinter.getInstance().printMessage(this.getClass(), "Merged: " + contacts);
            MessagesPrinter.getInstance().printMessage(this.getClass(), "Duplicates: " + contactsDuplicates);
        }

        return restEntities;
    }

    private List<Map<String, String>> mergeContactsFromControllers(List<AController> controllers){
        List<Map<String, String>> contacts = Collections.emptyList();
        for (int i = 0; i < controllers.size() - 1; i++) {

            AController controller1 = controllers.get(i);
            AController controller2 = controllers.get(i + 1);

            contacts = mergeContactsFromControllers(controller1, controller2);
        }
        return contacts;
    }

    private SimpleEntity importDataToControllers(List<AController> controllers, List<Map<String, String>> data){
        SimpleEntity resultEntity = new SimpleEntity();

        Map<String, Object> statisticData = new HashMap<>();
        List<Object> ids = new ArrayList<>();
        for (AController importController : controllers) {
            Map<String, Object> controllerEntity1 = importController.importContacts(data, null).collectData();
            Map<String, Object> controllerEntity2 = importController.importContacts(contactsDuplicates, null).collectData();

            statisticData.put("mergedFile", controllerEntity1.get("file"));
            statisticData.put("duplicateFile", controllerEntity2.get("file"));
            ids.add(importController.getId());

            writeImportLog(importController.getControllerName(), controllerEntity1);
            writeImportLog(importController.getControllerName(), controllerEntity2);
        }

        resultEntity.addData(statisticData);
        resultEntity.addData("merged", data.size());
        resultEntity.addData("duplicates", contactsDuplicates.size());
        resultEntity.addData("accounts", ids);

        return resultEntity;
    }


    private List<Map<String, String>> mergeContactsFromControllers(AController controller1, AController controller2) {

        List<Map<String, String>> data1 = controller1.exportContacts();
        List<Map<String, String>> data2 = controller2.exportContacts();

        Merger merger = new Merger(data1, data2);
        merger.merge(Arrays.asList(connectorPOJO.getMergeParams()));

        List<Map<String, String>> duplicates = CsvController.convertToListOfMaps(merger.getMergedDataDup());
        List<Map<String, String>> merged = CsvController.convertToListOfMaps(merger.getMergedDataNDup());

        contactsDuplicates.addAll(duplicates);

        writeMergeLog(controller1.getControllerName(), controller2.getControllerName(), merged.size(), duplicates.size());
        return merged;
    }


    private void writeMergeLog(String controller1, String controller2, int merged, int duplicates){
        String message = generateMessageHead();
        message += String.format("%s-%s", controller1, controller2);
        message += String.format("(merged=%s, duplicates=%s)", merged, duplicates);

        SimpleEntityDao.SimpleLog simpleLog1 = new SimpleEntityDao.SimpleLog();
        simpleLog1.setMessage(message);

        entityDao.writeToDb(simpleLog1);
    }
}
