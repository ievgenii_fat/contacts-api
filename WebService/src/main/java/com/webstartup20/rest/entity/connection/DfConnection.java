package com.webstartup20.rest.entity.connection;

import com.webstartup20.api_controller.AController;
import com.webstartup20.dao.SimpleEntityDao;
import com.webstartup20.entity.RestEntitiesArray;
import com.webstartup20.entity.RestEntitiesContainer;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.entity.pojo.ConnectorPOJO;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.*;

/**
 * Created by Sergiy on 21-Dec-14.
 */
public class DfConnection extends AConnection {

    private SimpleEntityDao.SimpleLog simpleLog;

    public DfConnection(ConnectorPOJO connectorPOJO) {
        super(connectorPOJO);
        simpleLog = new SimpleEntityDao.SimpleLog();
    }

    /**
     *
     * @param exportControllers
     * @param importControllers
     * @return
     */
    public RestEntity connect(List<AController> exportControllers, List<AController> importControllers){
        SimpleEntity simpleEntity = new SimpleEntity();

        simpleEntity.addData("id", new Integer(connectorPOJO.getId()));

        Set<Object> ids = new HashSet<>();
        Map<String, Object> data = new HashMap<>();
        for (AController exportController : exportControllers) {
            List<Map<String, String>> contacts = exportController.exportContacts();

            for (AController importController : importControllers) {
                FieldMapperBuilder fieldMapperBuilder = getFieldMapperBuilder(exportController, importController);

                Map<String, Object> report = importController.importContacts(contacts, fieldMapperBuilder).collectData();
                data.putAll(report);//TODO will be changed. Just for demo
                ids.add(importController.getId());

                writeImportLog(importController.getControllerName(), report);
            }
        }

        simpleEntity.addData(data);
        simpleEntity.addData("accounts", ids);

        return simpleEntity;
    }
}












