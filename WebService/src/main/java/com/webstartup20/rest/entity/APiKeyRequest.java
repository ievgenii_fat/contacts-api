package com.webstartup20.rest.entity;

import com.webstartup20.entity.RestEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by FAT on 17.12.2014.
 */
public class APiKeyRequest implements RestEntity {
    private String apiKey;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Map<String, Object> collectData() {
        Map<String, Object> data = new HashMap<>();
        data.put("apiKey", apiKey);
        return data;
    }
}
