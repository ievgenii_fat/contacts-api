package com.webstartup20.rest.entity;

import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sergiy on 03-Jan-15.
 */
public class UploadFileResponse extends SimpleEntity {
    private Map<String, Object> file;
    private List<String> header;

    public Map<String, Object> getFiles() {
        return file;
    }
    public void setFiles(Map<String, Object> files) {
        this.file = files;
    }

    public List<String> getHeader() {
        return header;
    }

    public void setHeader(List<String> header) {
        this.header = header;
    }

    @Override
    public Map<String, Object> collectData() {

        HashMap<String, Object> data = new HashMap<>(file);
        data.put("header", header);

        return data;
    }
}
