package com.webstartup20.rest.entity.connection;

import com.webstartup20.api_controller.AController;
import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.csv.CsvController;
import com.webstartup20.entity.RestEntitiesArray;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.entity.pojo.ConnectorPOJO;
import com.webstartup20.entity.pojo.MappingPojo;
import com.webstartup20.general_input.GeneralController;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.merger.Merger;
import com.webstartup20.parsers.ContactParser;
import com.webstartup20.struct.IContact;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.*;

/**
 * Created by Sergiy on 04-Jan-15.
 */
public class SdmConnection extends AConnection {

    public SdmConnection(ConnectorPOJO connectorPOJO) {
        super(connectorPOJO);
    }

    private  List<Map<String, String>> contactsDuplicates = new ArrayList<>();

    @Override
    public RestEntity connect(List<AController> exportControllers, List<AController> importControllers) {

        SimpleEntity restEntities = new SimpleEntity();

        restEntities.addData("id", new Integer(connectorPOJO.getId()));

        if(exportControllers.size() > 0){

            List<Map<String, String>> contacts = Collections.emptyList();

            for (int i = 0; i < exportControllers.size() - 1; i++) {

                AController controller1 = exportControllers.get(i);
                AController controller2 = exportControllers.get(i+1);

                contacts = mergeContactsFromControllers(controller1, controller2);
            }

            MessagesPrinter.getInstance().printMessage(this.getClass(), "Merged: "+contacts);
            MessagesPrinter.getInstance().printMessage(this.getClass(), "Duplicates: "+contactsDuplicates);

            RestEntitiesArray entitiesArray = new RestEntitiesArray("accounts");

            for (AController importController : importControllers) {
                SimpleEntity accountEntity = new SimpleEntity();
                FieldMapperBuilder fieldMapperBuilder = createFieldMapperBuilderFromGeneralFormat(importController);

                accountEntity.addData("id", importController.getId());
                accountEntity.addData("merged", importController.importContacts(contacts, fieldMapperBuilder));
                accountEntity.addData("duplicates", importController.importContacts(contactsDuplicates, fieldMapperBuilder));

                entitiesArray.addEntity(accountEntity);
            }
            restEntities.addData("accounts", entitiesArray.getEntities());
        }

        entityDao.closeConnection();
        return restEntities;
    }


    private List<Map<String, String>> mergeContactsFromControllers(AController controller1, AController controller2){

        List<Map<String, String>> contacts1 = getContactsInGeneralFormat(controller1);
        List<Map<String, String>> contacts2 = getContactsInGeneralFormat(controller2);

        Merger merger = new Merger(contacts1, contacts2);
        merger.merge(Arrays.asList(connectorPOJO.getMergeParams()));
        contactsDuplicates.addAll(CsvController.convertToListOfMaps(merger.getMergedDataDup()));

        return CsvController.convertToListOfMaps(merger.getMergedDataNDup());
    }

    private List<Map<String, String>> getContactsInGeneralFormat(AController controller){
        List<Map<String, String>> contacts = controller.exportContacts();
        FieldMapperBuilder fieldMapperBuilder = createFieldMapperBuilderToGeneralFormat(controller);

        ContactParser contactParser = ContactParser.build(
                controller.getControllerName(),
                ResourcesConstants.General.NAME,
                fieldMapperBuilder.getFieldsMappers()
        );
        contactParser.parseData(contacts);

        List<IContact> parsedContacts = contactParser.getContacts();
        if(parsedContacts.size() != 0){
            return ContactParser.convertToListOfMap(parsedContacts, ResourcesConstants.General.NAME);
        }else{
            return contacts;
        }
    }


    private FieldMapperBuilder createFieldMapperBuilderToGeneralFormat(AController source){
        return createFieldMapperBuilder(source, new GeneralController(source.getId()+1));
    }

    private FieldMapperBuilder createFieldMapperBuilderFromGeneralFormat(AController source){
        return createFieldMapperBuilder(new GeneralController(source.getId()+1), source);
    }

    private FieldMapperBuilder createFieldMapperBuilder(AController source, AController target){
        Map<Integer, String> controllers = generateIdNameMapForControllers(source, target);
        return new FieldMapperBuilder(createMappingArray(source, target), controllers);
    }

    private MappingPojo[] createMappingArray(AController source, AController target){
        MappingPojo[] mappings = new MappingPojo[ResourcesConstants.MAIN_FIELDS_SET.length];
        for (int i = 0; i < ResourcesConstants.MAIN_FIELDS_SET.length; i++) {
            MappingPojo mapping = new MappingPojo();
            mapping.setSourceIcon(source.getId());
            mapping.setTargetIcon(target.getId());
            mapping.setSourceField(ContactParser.getConstantName(ResourcesConstants.MAIN_FIELDS_SET[i], source.getControllerName()));
            mapping.setTargetField(ContactParser.getConstantName(ResourcesConstants.MAIN_FIELDS_SET[i], target.getControllerName()));
            mappings[i] = mapping;
        }
        return mappings;
    }
}
