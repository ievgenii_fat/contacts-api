package com.webstartup20.rest.entity;

import com.webstartup20.entity.RestEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by FAT on 14.12.2014.
 */
public class MailchimpListIds implements RestEntity {
    private Map<String, Object> listNameListIdMap;

    public Map<String, Object> getListNameListIdMap() {
        return listNameListIdMap;
    }

    public void setListNameListIdMap(Map<String, Object> listNameListIdMap) {
        this.listNameListIdMap = listNameListIdMap;
    }

    @Override
    public Map<String, Object> collectData() {
        return new HashMap<>(listNameListIdMap);
    }
}
