package com.webstartup20.rest.util;

import com.webstartup20.api_controller.AController;
import com.webstartup20.entity.pojo.AccountPojo;
import com.webstartup20.entity.pojo.ConnectorPOJO;

import java.util.ArrayList;
import java.util.List;

public class WorkflowProcessFactory {
    private ApiControllerFactory iApiControllerFactory = new ApiControllerFactory();

    public WorkflowProcess getWorkflowProcess(List<AccountPojo> from, List<AccountPojo> toAccounts, ConnectorPOJO connectorPOJO) {
        List<AController> fromControllers = generateIApiControllers(from);
        List<AController> toControllers = generateIApiControllers(toAccounts);

        WorkflowProcess workflowProcess = new WorkflowProcess();
        workflowProcess.setExportController(fromControllers);
        workflowProcess.setImportController(toControllers);
        workflowProcess.setConnection(ConnectionFactory.getConnection(connectorPOJO));

        return workflowProcess;
    }

    private List<AController> generateIApiControllers(List<AccountPojo> accounts){
        List<AController> apiControllers = new ArrayList<>();
        for (AccountPojo toAccount : accounts) {
            apiControllers.add(iApiControllerFactory.getIApiController(toAccount));
        }
        return apiControllers;
    }
}
