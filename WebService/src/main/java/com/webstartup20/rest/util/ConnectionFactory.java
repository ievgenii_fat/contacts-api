package com.webstartup20.rest.util;

import com.webstartup20.rest.entity.connection.AConnection;
import com.webstartup20.rest.entity.connection.DfConnection;
import com.webstartup20.rest.entity.connection.DmConnection;
import com.webstartup20.entity.pojo.ConnectorPOJO;

/**
 * Created by Sergiy on 21-Dec-14.
 */
public class ConnectionFactory {
    public static AConnection getConnection(ConnectorPOJO connector){
        switch (connector.getType()){
            case AConnection.DF_CONNECTION:
                return new DfConnection(connector);
            case AConnection.DM_CONNECTION:
                return new DmConnection(connector);
        }
        return null;
    }
}
