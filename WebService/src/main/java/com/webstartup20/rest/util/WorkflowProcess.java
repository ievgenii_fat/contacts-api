package com.webstartup20.rest.util;

import com.webstartup20.api_controller.AController;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.rest.entity.connection.AConnection;

import java.util.List;

public class WorkflowProcess {
    private List<AController> exportControllers;
    private List<AController> importControllers;
    private AConnection connection;

    public void setExportController(List<AController> exportController) {
        this.exportControllers = exportController;
    }

    public void setImportController(List<AController> importController) {
        this.importControllers = importController;
    }

    public void setConnection(AConnection connection) {
        this.connection = connection;
    }

    public RestEntity perform() {
        return connection.connect(exportControllers, importControllers);
    }

    @Override
    public String toString() {
        return "WorkflowProcess{" +
                "exportControllers=" + exportControllers +
                ", importControllers=" + importControllers +
                ", connection=" + connection +
                '}';
    }
}