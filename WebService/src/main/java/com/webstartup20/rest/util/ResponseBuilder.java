package com.webstartup20.rest.util;

import com.webstartup20.entity.RestEntity;

import javax.ws.rs.core.Response;

/**
 * Created by FAT on 14.12.2014.
 */
public class ResponseBuilder {
    public Response buildResponse(RestEntity entity) {
        return Response.status(201).entity(entity).build();
    }
    public Response buildResponse(String entity) {
        return Response.status(201).entity(entity).build();
    }
}
