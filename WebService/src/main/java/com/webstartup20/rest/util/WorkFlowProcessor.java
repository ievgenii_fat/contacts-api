package com.webstartup20.rest.util;

import com.webstartup20.entity.pojo.AccountPojo;
import com.webstartup20.entity.pojo.ConnectorPOJO;
import com.webstartup20.entity.pojo.WorkflowPOJO;
import com.webstartup20.io.MessagesPrinter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergiy on 20-Dec-14.
 */
public class WorkFlowProcessor {

    private WorkflowPOJO workflowPOJO;
    private WorkflowProcessFactory workflowProcessFactory = new WorkflowProcessFactory();

    public WorkFlowProcessor(WorkflowPOJO workflowPOJO) {
        this.workflowPOJO = workflowPOJO;
    }

    public List<WorkflowProcess> determineProcesses() {
        List<WorkflowProcess> processes = new ArrayList<>();

        for (ConnectorPOJO connectorPOJO : workflowPOJO.getConnectors()) {

            List<AccountPojo> exportAccounts = getAccountsOfConnections(connectorPOJO.getIncomingConnections());
            List<AccountPojo> importAccounts = getAccountsOfConnections(connectorPOJO.getOutcomingConnections());

            processes.add(workflowProcessFactory.getWorkflowProcess(exportAccounts, importAccounts, connectorPOJO));
        }

        return processes;
    }

    private List<AccountPojo> getAccountsOfConnections(String[] connections){
        List<AccountPojo> importAccounts = new ArrayList<>();

        for (String accountId : connections) {
            try{
                AccountPojo account = getAccountById(Integer.parseInt(accountId));
                if(account == null){
                    throw new Exception("incorrect json, but I'll try to finish it");
                }
                importAccounts.add(account);
            }catch (Exception e){
                MessagesPrinter.getInstance().printErrorMessage(this.getClass(), e.getMessage());
            }
        }

        return importAccounts;
    }

    private AccountPojo getAccountById(int id){
        for (AccountPojo account : workflowPOJO.getAccounts()) {
            if(account.getId() == id){
                return account;
            }
        }
        return null;
    }
}
