package com.webstartup20.rest.util;

import com.webstartup20.entity.pojo.AccountPojo;
import com.webstartup20.entity.pojo.ConnectorPOJO;
import com.webstartup20.entity.pojo.WorkflowEntity;
import com.webstartup20.entity.pojo.WorkflowPOJO;

/**
 * Created by FAT on 21.12.2014.
 */
public class WorkflowEntityDefiner {
    private WorkflowPOJO workflowPOJO;

    public WorkflowEntityDefiner(WorkflowPOJO workflowPOJO) {
        this.workflowPOJO = workflowPOJO;
    }

    public WorkflowEntity getWorkflowEntity(int id) {
        ConnectorPOJO connectorPOJO = getConnectorPOJO(id);
        if (connectorPOJO != null) {
            return connectorPOJO;
        }
        AccountPojo accountsPojo = getAccountsPojo(id);
        if (accountsPojo != null) {
            return accountsPojo;
        }
        throw new RuntimeException("Empty workflow");
    }

    public ConnectorPOJO getConnectorPOJO(int id) {
        for (ConnectorPOJO connectorPOJO : workflowPOJO.getConnectors()) {
            if (connectorPOJO.getId() == id) {
                return connectorPOJO;
            }
        }
        return null;
    }

    public AccountPojo getAccountsPojo(int id) {
        for (AccountPojo accountsPojo : workflowPOJO.getAccounts()) {
            if (accountsPojo.getId() == id) {
                return accountsPojo;
            }
        }
        return null;
    }
}
