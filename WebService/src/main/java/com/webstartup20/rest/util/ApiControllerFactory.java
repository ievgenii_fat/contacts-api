package com.webstartup20.rest.util;

import com.webstartup20.MailChimpController;
import com.webstartup20.api_controller.AController;
import com.webstartup20.constants.PathConstants;
import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.csv.CsvDownloadController;
import com.webstartup20.csv.CsvUploadController;
import com.webstartup20.googlecontacts.GoogleController;
import com.webstartup20.entity.pojo.AccountPojo;

/**
 * Created by FAT on 21.12.2014.
 */
public class ApiControllerFactory {
    public AController getIApiController(AccountPojo accountsPojo) {

        switch (accountsPojo.getType()){
            case ResourcesConstants.Google.NAME:
                String login = accountsPojo.getLoginInfo().getUsername();
                String password = accountsPojo.getLoginInfo().getPassword();
                String accessToken = accountsPojo.getLoginInfo().getToken();
                GoogleController googleController = new GoogleController(accountsPojo.getId(),accessToken);
                return googleController;
            case ResourcesConstants.Mailchimp.NAME:
                String apikey = accountsPojo.getLoginInfo().getAPI();
                String listId = accountsPojo.getLoginInfo().getListId();
                MailChimpController mailChimpController = new MailChimpController(accountsPojo.getId(),apikey, listId);
                return mailChimpController;
            case ResourcesConstants.CsvUpload.NAME:
                String filePath = PathConstants.UPLOAD_DIR_PATH + accountsPojo.getLoginInfo().getFileName();
                return new CsvUploadController(accountsPojo.getId(), filePath);
            case ResourcesConstants.CsvDownload.NAME:
                return new CsvDownloadController(accountsPojo.getId());
        }

        return null;
    }
}
