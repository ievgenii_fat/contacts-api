package com.webstartup20.rest.api;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.webstartup20.constants.PathConstants;
import com.webstartup20.csv.CsvUtil;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.rest.entity.UploadFileResponse;
import com.webstartup20.struct.FileUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.*;

/**
 * Created by Sergiy on 26-Dec-14.
 */
@Path("/file/")
public class FileUploaderApi extends AbstractApi {


    private List<String> headers;

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {

        String fileName = fileDetail.getFileName();

        MessagesPrinter.getInstance().printMessage(this.getClass(), String.format("File '%s' is uploading.", fileName));

        if (!fileName.endsWith(CsvUtil.FORMAT)) {
            return Response.status(400).entity("Only 'csv' files are allowed").build();
        }

        String stampedFileName = createStampedFileName();

        if (writeToFile(inputStream, PathConstants.UPLOAD_DIR_PATH + stampedFileName)) {
            MessagesPrinter.getInstance().printMessage(this.getClass(), String.format("File saved as '%s'", stampedFileName));
            return toResponse(createEntity(fileName, stampedFileName));
        } else {
            return Response.status(500).entity("Server error while saving the file").build();
        }
    }

    private RestEntity createEntity(String oldFileName, String newFileName) {
        UploadFileResponse uploadFileResponse = new UploadFileResponse();
        Map<String, Object> file = new HashMap<>();
        file.put(oldFileName, newFileName);
        uploadFileResponse.setFiles(file);
        uploadFileResponse.setHeader(headers);
        return uploadFileResponse;
    }

    private String createStampedFileName() {
        return FileUtils.getFileNameTimeStamp() + CsvUtil.FORMAT;
    }

    private boolean writeToFile(InputStream inputStream, String filePath) {
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        try (Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF8"))) {
            boolean headerSaved = false;
            String row;
            while ((row = br.readLine()) != null) {
                MessagesPrinter.getInstance().printMessage(this.getClass(), String.format("Row '%s' is saving to file %s.", row, filePath));
                if (!headerSaved) {
                    headerSaved |= storeHeader(row);
                }
                out.write(row + "\n");
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeStream(br);
        }
    }

    private void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean storeHeader(String row) {
        if (row.replaceAll(" ", "").isEmpty()) {
            return false;
        }
        headers = new ArrayList<>(Arrays.asList(row.split(CsvUtil.DELIM)));
        return true;
    }
}






























