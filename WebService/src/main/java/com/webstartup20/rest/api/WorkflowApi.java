package com.webstartup20.rest.api;

import com.webstartup20.entity.RestEntitiesArray;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.pojo.WorkflowPOJO;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.rest.util.WorkFlowProcessor;
import com.webstartup20.rest.util.WorkflowProcess;
import com.webstartup20.struct.FileUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by FAT on 21.12.2014.
 */
@Path("/workflowapi/")
public class WorkflowApi extends AbstractApi {

    @POST
    @Path("/workflow")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response processContacts(WorkflowPOJO workflowPOJO, @Context HttpServletRequest req) {

        MessagesPrinter.getInstance().printMessage(this.getClass(), String.format("\nTime: %s\nIP: %s\nJSON: %s",
                FileUtils.getFileNameTimeStamp(), req.getRemoteAddr(), workflowPOJO));
        try {
            WorkFlowProcessor workFlowProcessor = new WorkFlowProcessor(workflowPOJO);
            List<WorkflowProcess> workflowProcesses = workFlowProcessor.determineProcesses();

            RestEntitiesArray restEntitiesArray = new RestEntitiesArray("connectors");

            for (WorkflowProcess workflowProcess : workflowProcesses) {
                RestEntity restEntity = workflowProcess.perform();
                restEntitiesArray.addEntity(restEntity);
            }

            return toResponse(restEntitiesArray);
//            return Response.status(201).entity("Success").build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(400).entity("Error occurred").build();
        }
    }

}
