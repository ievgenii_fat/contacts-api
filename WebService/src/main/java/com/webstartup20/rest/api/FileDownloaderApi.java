package com.webstartup20.rest.api;

import com.webstartup20.constants.PathConstants;
import com.webstartup20.io.MessagesPrinter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

@Path("/files")
public class FileDownloaderApi {

//    @GET
//    @Path("/download")
//    @Produces("text/plain")
//    public Response downloadFile(@QueryParam("file") String fileName) {
//        String filePath = PathConstants.DOWNLOAD_DIR_PATH +fileName;
//
//        File file = new File(filePath);
//
//        Response.ResponseBuilder response = Response.ok(file);
//        String contentDisposition = String.format("attachment; filename=\"%s\"", filePath);
//        response.header("Content-Disposition", contentDisposition);
//
//        MessagesPrinter.getInstance().printMessage(this.getClass(), String.format("File %s was downloaded.",fileName));
//
//        return response.build();
//    }

    @GET
    @Path("/download")
    @Produces("text/plain")
    public Response download(@QueryParam("file") String fileName,
                             @Context HttpServletResponse response) throws IOException {

        File file = new File(PathConstants.DOWNLOAD_DIR_PATH + fileName);

        response.setContentLength((int)file.length());
        response.setHeader("Content-Disposition", "attachment; filename="
                + fileName);

        ServletOutputStream outStream = response.getOutputStream();
        byte[] bbuf = new byte[(int) file.length() + 1024];
        DataInputStream in = new DataInputStream(new FileInputStream(file));
        int length;
        while ((in != null) && ((length = in.read(bbuf)) != -1)) {
            outStream.write(bbuf, 0, length);
        }
        in.close();
        outStream.flush();

        MessagesPrinter.getInstance().printMessage(this.getClass(), String.format("'%s' was downloaded.", fileName));
        file.delete();
        MessagesPrinter.getInstance().printMessage(this.getClass(), String.format("'%s' was deleted.", fileName));
        return Response.ok().build();
    }

}

