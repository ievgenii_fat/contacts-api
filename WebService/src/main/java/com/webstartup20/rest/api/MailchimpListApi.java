package com.webstartup20.rest.api;

import com.webstartup20.mailchimp.MailchimpUtil;
import com.webstartup20.rest.entity.APiKeyRequest;
import com.webstartup20.rest.entity.MailchimpListIds;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by FAT on 14.12.2014.
 */
@Path("/contactsapi/")
public class MailchimpListApi extends AbstractApi {

    @POST
    @Path("/listids")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListIds(APiKeyRequest aPiKeyRequest) {
        MailchimpListIds mailchimpListIds = new MailchimpListIds();
        mailchimpListIds.setListNameListIdMap(MailchimpUtil.getListsList(aPiKeyRequest.getApiKey()));
        return toResponse(mailchimpListIds);
    }
}
