package com.webstartup20.rest.api;

import com.webstartup20.entity.RestEntity;
import com.webstartup20.rest.util.ResponseBuilder;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by FAT on 14.12.2014.
 */
public abstract class AbstractApi {
    protected ResponseBuilder responseBuilder = new ResponseBuilder();

    private ObjectMapper mapper = new ObjectMapper();

    protected String mapToJson(Object o) {
        try {
          return mapper.writeValueAsString(o);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    protected Response toResponse(RestEntity restEntity) {
        String json = mapToJson(restEntity);
        return responseBuilder.buildResponse(json);
    }
}
