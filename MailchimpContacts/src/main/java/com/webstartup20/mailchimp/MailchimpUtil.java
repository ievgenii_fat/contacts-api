package com.webstartup20.mailchimp;

import com.ecwid.mailchimp.MailChimpClient;
import com.ecwid.mailchimp.MailChimpException;
import com.ecwid.mailchimp.MailChimpMethod;
import com.ecwid.mailchimp.method.v2_0.lists.Email;
import com.ecwid.mailchimp.method.v2_0.lists.SubscribeMethod;
import com.webstartup20.mailchimp.methods.ListMethod;
import com.webstartup20.mailchimp.methods.MembersMethod;
import com.webstartup20.mailchimp.methods.PingMethod;
import com.webstartup20.mailchimp.structs.*;
import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.struct.IContact;

import java.io.IOException;
import java.util.*;

/**
 * Created by Sergiy on 01-Oct-14.
 */
public class MailchimpUtil {

    private static final String LIST_ID_ERROR = "id_error";

    private String apiKey;
    private MailChimpClient mailChimpClient;

    public MailchimpUtil() {
        mailChimpClient = new MailChimpClient();
    }

    public MailchimpUtil(String apiKey) {
        this();
        this.apiKey = apiKey;
    }

    public String ping() {
        PingMethod pingMethod = new PingMethod();
        pingMethod.apikey = this.apiKey;
        return ((PingResult) execute(pingMethod)).msg;
    }


    public void addNewContact(String listId, String firstName, String lastName, String email) {
        SubscribeMethod subscribeMethod = new SubscribeMethod();
        subscribeMethod.apikey = this.apiKey;
        subscribeMethod.id = listId;
        subscribeMethod.email = new Email();
        subscribeMethod.email.email = email;
        subscribeMethod.double_optin = false;
        subscribeMethod.update_existing = true;
        subscribeMethod.merge_vars = new MailchimpContact(email, firstName, lastName);

        execute(subscribeMethod);
    }

    public void addContacts(String listId, List<IContact> datas) {
        for (IContact contactData : datas) {
            addNewContact(listId, contactData);
        }
    }

    public void addNewContact(String listId, IContact mergeVars) {
        for (String email : mergeVars.getEmails()) {
            addNewContact(listId, mergeVars.getFirstName(), mergeVars.getLastName(), email);
            return;
        }
    }

    public List<Map<String, String>> getContactsFromList(String listId) {
        if (listId.equals(LIST_ID_ERROR)) {
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(), "No one list was created or incorrect list id.");
            return Collections.emptyList();
        }
        MembersMethod membersMethod = new MembersMethod();
        membersMethod.id = listId;
        membersMethod.apikey = apiKey;
        MembersResult membersResult = (MembersResult) execute(membersMethod);
        return convertToContactDataList(membersResult.data);
    }

    public List<ListResult.Data> getListsList() {
        ListMethod listMethod = new ListMethod();
        listMethod.apikey = apiKey;

        ListResult listResult = (ListResult) execute(listMethod);

        return listResult.data;
    }

    public List<Map<String, String>> getAllContacts() {
        List<Map<String, String>> contacts = new ArrayList<>();

        for (ListResult.Data listId : getListsList()) {
            contacts.addAll(getContactsFromList(listId.id));
        }

        return contacts;
    }


    private Object execute(MailChimpMethod<?> method) {
        MailChimpClient mailChimpClient = new MailChimpClient();
        try {
            return mailChimpClient.execute(method);
        } catch (IOException e) {
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(), "Data reading error. Check your Internet connection");
        } catch (MailChimpException e) {
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(), e.getLocalizedMessage());
        } finally {
            mailChimpClient.close();
        }
        return null;
    }

    private IContact convertMyMemberInfoDataToContactData(MyMemberInfoData myMemberInfoData) {
        MailchimpContact contactData = new MailchimpContact();
        contactData.EMAIL = myMemberInfoData.email;
        contactData.FNAME = myMemberInfoData.merges.FNAME;
        contactData.LNAME = myMemberInfoData.merges.LNAME;
        return contactData;
    }

    private List<Map<String, String>> convertToContactDataList(List<MyMemberInfoData> list) {
        List<Map<String, String>> resultList = new ArrayList<>();
        for (MyMemberInfoData myMemberInfoData : list) {
            Map<String, String> datas = new HashMap<>();
            datas.put(ResourcesConstants.Mailchimp.HEADER[0], myMemberInfoData.merges.FNAME);
            datas.put(ResourcesConstants.Mailchimp.HEADER[1], myMemberInfoData.merges.LNAME);
            datas.put(ResourcesConstants.Mailchimp.HEADER[2], myMemberInfoData.email);
            resultList.add(datas);
        }
        return resultList;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }


    public static Map<String, Object> getListsList(String apiKey) {
        MailchimpUtil mailchimpUtil = new MailchimpUtil(apiKey);
        try {
            mailchimpUtil.ping();
        } catch (Exception e) {
            return null;
        }

        Map<String, Object> lists = new HashMap<>();

        for (ListResult.Data listData : mailchimpUtil.getListsList()) {
            lists.put(listData.id, listData.name);
        }

        return lists;
    }

}