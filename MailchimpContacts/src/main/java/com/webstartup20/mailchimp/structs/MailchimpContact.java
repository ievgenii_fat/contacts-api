package com.webstartup20.mailchimp.structs;

import com.ecwid.mailchimp.MailChimpObject;
import com.webstartup20.struct.IContact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergiy on 01-Oct-14.
 */
public class MailchimpContact extends MailChimpObject implements IContact {

    @Field
    public String EMAIL, FNAME, LNAME;

    public MailchimpContact(){}

    public MailchimpContact(String email, String fname, String lname) {
        this.EMAIL = email;
        this.FNAME = fname;
        this.LNAME = lname;
    }

    @Override
    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(EMAIL);
        return emails;
    }

    @Override
    public String getFirstName() {
        return FNAME;
    }

    @Override
    public String getLastName() {
        return LNAME;
    }
}