package com.webstartup20.mailchimp.methods;

import com.ecwid.mailchimp.MailChimpAPIVersion;
import com.ecwid.mailchimp.MailChimpMethod;
import com.webstartup20.mailchimp.structs.PingResult;

/**
 * Created by Sergiy on 01-Oct-14.
 */
@MailChimpMethod.Method(name = "/helper/ping", version = MailChimpAPIVersion.v2_0)
public class PingMethod extends MailChimpMethod<PingResult> {
}
