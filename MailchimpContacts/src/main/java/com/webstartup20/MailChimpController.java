package com.webstartup20;

import com.webstartup20.api_controller.exception.InvalidControllerNameException;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.mailchimp.MailchimpUtil;
import com.webstartup20.api_controller.AController;
import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.entity.SimpleEntity;
import com.webstartup20.parsers.ContactParser;
import com.webstartup20.struct.IContact;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailChimpController extends AController {

    private String apikey;
    private String listId;

    private MailchimpUtil mailchimpUtil;


    public MailChimpController(int id, String apiKey, String listId) {
        super(id);
        this.apikey = apiKey;
        this.listId = listId;
        mailchimpUtil = new MailchimpUtil(apiKey);
    }

    @Override
    public RestEntity importContacts(List<Map<String, String>> contacts, FieldMapperBuilder fieldMapperBuilder) {

        String exportControllerName;
        try {
            exportControllerName = determineExportController(fieldMapperBuilder);
        } catch (InvalidControllerNameException e) {
            int errorId = MessagesPrinter.getInstance().printErrorMessage(this.getClass(), e.getMessage());
            String errorMessage = String.format("[ID: %s] Internal server error. Please inform your administrator!", errorId);
            return generateResponse(errorMessage, 0, 0);
        }

        ContactParser contactParser = ContactParser.build(
                exportControllerName,
                getControllerName(),
                fieldMapperBuilder.getFieldsMappers());

        contactParser.parseData(contacts);
        List<IContact> parsedContacts = contactParser.getContacts();

        mailchimpUtil.addContacts(listId, parsedContacts);

        return generateResponse("success", parsedContacts.size(), contacts.size() - parsedContacts.size());
    }

    @Override
    public List<Map<String, String>> exportContacts() {
       return mailchimpUtil.getContactsFromList(listId);
    }

    @Override
    public String getControllerName() {
        return ResourcesConstants.Mailchimp.NAME;
    }

}