package com.webstartup20.parsers;

import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.utils.field_map.FieldsMapper;

import java.util.List;

/**
 * Created by Sergiy on 10-Oct-14.
 */
public class GeneralContactParser extends ContactParser {

    public static final String FIRST_NAME = ResourcesConstants.General.HEADER[0];
    public static final String FAMILY_NAME = ResourcesConstants.General.HEADER[1];
    public static final String EMAILS = ResourcesConstants.General.HEADER[2];


    public GeneralContactParser(List<FieldsMapper> fieldMapperList, String inputDataName) {
        super(fieldMapperList, inputDataName);
    }

    @Override
    protected String getCorrectKey(String _key, String inputDataName){
        for (FieldsMapper fieldsMapper:fieldMapperList){
            if(equalsIgnoreCaseAndSpaces(fieldsMapper.getFieldNameFor(inputDataName).getName(), _key)){
                return fieldsMapper.getGeneralFieldName().getName();
            }
        }
        return null;
    }

    @Override
    protected boolean isSkipIfEmptyForKey(String key) {
        for (FieldsMapper fieldsMapper: fieldMapperList){
            if(fieldsMapper.getGeneralFieldName().getName().equalsIgnoreCase(key)){
                return fieldsMapper.isSkip();
            }
        }
        return false;
    }

    @Override
    protected String getFirstNameConstant() {
        return FIRST_NAME;
    }

    @Override
    protected String getFamilyNameConstant() {
        return FAMILY_NAME;
    }

    @Override
    protected String getEmailConstant() {
        return EMAILS;
    }
}
