package com.webstartup20.parsers;

import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.utils.field_map.FieldsMapper;
import com.google.gdata.data.extensions.*;

import java.util.List;

/**
 * Created by Sergiy on 06-Oct-14.
 */
public class GoogleContactsParser extends ContactParser{

    public static final String FIRST_NAME = ResourcesConstants.Google.HEADER[2];
    public static final String FAMILY_NAME = ResourcesConstants.Google.HEADER[4];
    public static final String EMAILS = ResourcesConstants.Google.HEADER[6];

    public GoogleContactsParser(List<FieldsMapper> fieldMapperList,String inputDataName){
        super( fieldMapperList, inputDataName);
    }

    protected boolean isSkipIfEmptyForKey(String key){
        for (FieldsMapper fieldsMapper: fieldMapperList){
            if(fieldsMapper.getGoogleFieldName().getName().equalsIgnoreCase(key)){
                return fieldsMapper.isSkip();
            }
        }
        return false;
    }

    @Override
    protected String getCorrectKey(String _key, String inputDataName){
        for (FieldsMapper fieldsMapper:fieldMapperList){
            if(equalsIgnoreCaseAndSpaces(fieldsMapper.getFieldNameFor(inputDataName).getName(), _key)){
                return fieldsMapper.getGoogleFieldName().getName();
            }
        }
        return null;
    }

    @Override
    protected String getFirstNameConstant() {
        return FIRST_NAME;
    }

    @Override
    protected String getFamilyNameConstant() {
        return FAMILY_NAME;
    }

    @Override
    protected String getEmailConstant() {
        return EMAILS;
    }


//    public List<ContactEntry> getContacts() {
//        List<ContactEntry> contactEntries = new ArrayList<>();
//        for (Contact contact:contacts){
//            ContactEntry contactEntry = new ContactEntry();
//            Name name = createName(contact.getFirstName(), contact.getLastName());
//            contactEntry.setName(name);
//            boolean primaryEmailCreated = false;
//            for (String email:contact.getEmails()){
//                if(primaryEmailCreated){
//                    contactEntry.addEmailAddress(createEmail(email, name));
//                }else{
//                    contactEntry.addEmailAddress(createPrimaryEmail(email, name));
//                }
//
//            }
//            contactEntries.add(contactEntry);
//        }
//
//        return contactEntries;
//    }

    private Name createName(String givenName, String familyName){
        Name name = new Name();
        name.setFullName(new FullName(givenName+" "+familyName, NO_YOMI));
        name.setGivenName(new GivenName(givenName, NO_YOMI));
        name.setFamilyName(new FamilyName(familyName, NO_YOMI));
        return name;
    }

    private Email createEmail(String emailName, Name displayName){
        Email email = createEmail(emailName);
        email.setDisplayName(displayName.getFullName().toString());
        return email;
    }

    private Email createPrimaryEmail(String email, Name displayName){
        Email primaryMail = createEmail(email, displayName);
        primaryMail.setPrimary(true);
        return primaryMail;
    }

    private Email createEmail(String value){
        Email email = new Email();
        email.setAddress(value);
        email.setRel("http://schemas.google.com/g/2005#other");
        return email;
    }


}
