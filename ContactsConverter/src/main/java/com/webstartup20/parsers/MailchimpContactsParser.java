package com.webstartup20.parsers;

import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.utils.field_map.FieldsMapper;

import java.util.List;

/**
 * Created by Sergiy on 06-Oct-14.
 */
public class MailchimpContactsParser extends ContactParser{

    public static final String FIRST_NAME = ResourcesConstants.Mailchimp.HEADER[0];
    public static final String FAMILY_NAME = ResourcesConstants.Mailchimp.HEADER[1];
    public static final String EMAILS = ResourcesConstants.Mailchimp.HEADER[2];

    public MailchimpContactsParser(List<FieldsMapper> fieldMapperList, String inputDataName){
        super(fieldMapperList, inputDataName);
    }
    protected boolean isSkipIfEmptyForKey(String key){
        for (FieldsMapper fieldsMapper: fieldMapperList){
            if(fieldsMapper.getMailChimpFieldName().getName().equalsIgnoreCase(key)){
                return fieldsMapper.isSkip();
            }
        }
        return false;
    }

    @Override
    protected String getCorrectKey(String _key, String inputDataName){
        for (FieldsMapper fieldsMapper:fieldMapperList){
            if(equalsIgnoreCaseAndSpaces(fieldsMapper.getFieldNameFor(inputDataName).getName(), _key)){
                return fieldsMapper.getMailChimpFieldName().getName();
            }
        }
        return null;
    }

    @Override
    protected String getFirstNameConstant() {
        return FIRST_NAME;
    }

    @Override
    protected String getFamilyNameConstant() {
        return FAMILY_NAME;
    }

    @Override
    protected String getEmailConstant() {
        return EMAILS;
    }


//    public List<MailchimpContact> getContacts() {
//
//        List<MailchimpContact> mailChimpContacts = new ArrayList<>();
//        for (Contact contact:contacts){
//            MailchimpContact mailchimpContact = new MailchimpContact();
//            mailchimpContact.FNAME = contact.getFirstName();
//            mailchimpContact.LNAME = contact.getLastName();
//            for (String email:contact.getEmails()){
//                mailchimpContact.EMAIL = email;
//                break;
//            }
//            mailChimpContacts.add(mailchimpContact);
//        }
//
//        return mailChimpContacts;
//    }
}
