package com.webstartup20.utils;

import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.utils.field_map.FieldsMapper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sergiy on 06-Oct-14.
 */
public class FieldMapperUtil {


    private static final String GOOGLE_COLUMN_NAME = "google";
    private static final String MAILCHIMP_COLUMN_NAME = "mailchimp";
    private static final String GENERAL_COLUMN_NAME = "general";
    private static final String SKIP_OPTION_COLUMN_NAME = "field check options";
    private static final String EMPTY_COLUMN = "";
    private static final String[] HEADER_NAMES = {GOOGLE_COLUMN_NAME, MAILCHIMP_COLUMN_NAME, GENERAL_COLUMN_NAME, SKIP_OPTION_COLUMN_NAME, EMPTY_COLUMN};


    private static final String SKIP_YES = "yes";
    private static final String SKIP_NO = "no";
    private static final String SEPARATOR = ",";

    private Map<Integer, String> header;
    private List<FieldsMapper> fieldsMapperList;
    private String mapFilePath;

    public FieldMapperUtil(String mapFilePath){
        this.mapFilePath = mapFilePath;
        header = new HashMap<>();
        fieldsMapperList = new ArrayList<>();
        parseMapFile();
    }

    private void parseMapFile(){
        try(BufferedReader br = new BufferedReader(new FileReader(mapFilePath))) {
            String line;
            processHeader(br.readLine());
            while ((line = br.readLine()) != null) {

                parseFileRow(line);
            }
        }catch (FileNotFoundException e){
            String errorMes = String.format("File '%s' not found", mapFilePath);
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(),errorMes);
        }catch (IOException e){
            String errorMes = String.format("Error during reading file '%s'", mapFilePath);
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(),errorMes);
        }
    }

    protected void parseFileRow(String row){
        String[] splittedRow = row.split(SEPARATOR);

        FieldsMapper fieldsMapper = new FieldsMapper();
        FieldsMapper.GoogleFieldName googleFieldName = null;
        FieldsMapper.MailChimpFieldName mailChimpFieldName = null;
        FieldsMapper.GeneralFieldName generalFieldName = null;
        boolean skip = false;

        for (int i = 0; i < splittedRow.length; i++) {
            if(isEmptyCell(splittedRow[i])){
                return;
            }
            switch (header.get(i)){
                case GOOGLE_COLUMN_NAME:
                    googleFieldName = new FieldsMapper.GoogleFieldName(splittedRow[i]);
                    break;
                case MAILCHIMP_COLUMN_NAME:
                    mailChimpFieldName = new FieldsMapper.MailChimpFieldName(splittedRow[i]);
                    break;
                case GENERAL_COLUMN_NAME:
                    generalFieldName = new FieldsMapper.GeneralFieldName(splittedRow[i]);
                    break;
                case SKIP_OPTION_COLUMN_NAME:
                    skip = determineSkipOption(splittedRow[i]);
                    break;
                case EMPTY_COLUMN:
                    break;
            }
        }

        if(googleFieldName == null || mailChimpFieldName == null || generalFieldName == null){
            return;
        }
        fieldsMapper.setGoogleFieldName(googleFieldName);
        fieldsMapper.setMailChimpFieldName(mailChimpFieldName);
        fieldsMapper.setGeneralFieldName(generalFieldName);
        fieldsMapper.setSkip(skip);

        fieldsMapperList.add(fieldsMapper);
    }

    private boolean determineSkipOption(String value){
        return value.equalsIgnoreCase(SKIP_YES);
    }

    private boolean isEmptyCell(String cell){
        String buff = cell.replace(SEPARATOR, "");
        buff = buff.replace(" ", "");
        return buff.length() == 0;
    }


    private void processHeader(String row){
        String[] headerColumns = row.split(SEPARATOR);

        for (int i = 0; i < headerColumns.length; i++) {
            if(containsValue(headerColumns[i], HEADER_NAMES)){
                header.put(i, headerColumns[i].toLowerCase());
            }else if(headerColumns[i].replace(SEPARATOR,"").length() <= 0){
                header.put(i, "");
            }else{
                MessagesPrinter.getInstance().printErrorMessage(this.getClass(),String.format("'%s' field is not determined", headerColumns[i]));
            }
        }
    }

    private boolean containsValue(String value, String[] array){
        for (int i = 0; i < array.length; i++) {
            if(array[i].equalsIgnoreCase(value)){
                return true;
            }
        }
        return false;
    }

    public List<FieldsMapper> getFieldsMapperList() {
        return fieldsMapperList;
    }
}
