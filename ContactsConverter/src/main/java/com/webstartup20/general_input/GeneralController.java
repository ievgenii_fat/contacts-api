package com.webstartup20.general_input;

import com.webstartup20.api_controller.AController;
import com.webstartup20.api_controller.exception.InvalidControllerNameException;
import com.webstartup20.constants.ResourcesConstants;
import com.webstartup20.entity.RestEntity;
import com.webstartup20.io.MessagesPrinter;
import com.webstartup20.parsers.ContactParser;
import com.webstartup20.utils.field_map.FieldMapperBuilder;

import java.util.List;
import java.util.Map;

/**
 * Created by Sergiy on 10-Oct-14.
 */
public class GeneralController extends AController {

    private List<Map<String, String>> contacts;

    public GeneralController(int id) {
        super(id);
    }

    @Override
    public RestEntity importContacts(List<Map<String, String>> contacts, FieldMapperBuilder fieldMapperBuilder) {

        String exportControllerName;
        try {
            exportControllerName = determineExportController(fieldMapperBuilder);
        } catch (InvalidControllerNameException e) {
            MessagesPrinter.getInstance().printErrorMessage(this.getClass(), e.getMessage());
            return null;// TODO error message response
        }

        ContactParser contactParser = ContactParser.build(
                exportControllerName,
                getControllerName(),
                fieldMapperBuilder.getFieldsMappers()
        );
        contactParser.parseData(contacts);

        contacts = ContactParser.convertToListOfMap(contactParser.getContacts(), exportControllerName);

        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<Map<String, String>> exportContacts() {
        return contacts;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getControllerName() {
        return ResourcesConstants.General.NAME;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
